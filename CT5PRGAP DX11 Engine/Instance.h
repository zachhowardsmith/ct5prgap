#pragma once
#pragma pack(push)
#pragma pack(1)

#pragma region Custom Includes
#include "Globals.h"
#include "Vector3.h"
#pragma endregion

#pragma region System Includes
#include <xnamath.h>
#pragma endregion

using namespace Globals::TypeDefinitions;

class Instance
{
#pragma region Public Functions
public:
	Instance();
	~Instance();

#pragma region Private Functions
#pragma endregion

#pragma region Public Variables
	XMMATRIX matrix;
	int textureID;
#pragma endregion	
};
#pragma pack(pop)
