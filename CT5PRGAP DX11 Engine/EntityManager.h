#pragma once
#pragma region System Includes
#include <map>
#pragma endregion
#pragma region Custom Includes
#include "DXManager.h"
#include "Entity.h"
#include "Mesh.h"
#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "GeometryShader.h"
#include "PixelShader.h"
#pragma endregion

using namespace Shaders;
class EntityManager
{

#pragma region Public Functions
public:
	EntityManager();
	~EntityManager();

	bool AddEntity(std::string tag, 
		Mesh* mesh, 
		VertexShader* vertexShader, 
		HullShader* hullShader,
		DomainShader* domainShader,
		GeometryShader* geometryShader,
		PixelShader* pixelShader);

	Entity* operator[](LPCSTR tag);
#pragma endregion
#pragma region Private Functions
private:
	//Miscellaneous
#pragma endregion

#pragma region Public Variables
public:
	std::map<std::string, Entity> entities;
#pragma endregion
#pragma region Private Variables
private:
#pragma endregion
};

