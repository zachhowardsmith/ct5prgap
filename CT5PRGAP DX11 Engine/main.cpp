#include <windows.h>    // include the basic windows header file
#include "GameWindow.h"

//My Prototypes
GameWindow* window;
GameManager* game;

void LoadResources();
void LoadShaders();
void LoadMeshes();
void LoadTextures();
void LoadEntities();
void AttachConsole();
GameState RunFrame(MSG* msg);
void HandleInput(MSG* msg);
void RunParamaters();
void HandleCursorPosition();
void GetResolution(int* width, int* height);

//My declarations
GameState gameState = Initialize;

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	AttachConsole();
	window = new GameWindow(LoadResources, RunFrame);
	return window->Construct(hInstance, hPrevInstance, lpCmdLine, nCmdShow, &game);
}

void LoadResources()
{
	LoadShaders();
	LoadMeshes();
	LoadTextures();
	LoadEntities();
}

void LoadShaders()
{
	Print("Loading Shaders");

	game->LoadShader("Resources/Shaders/VertexShader.fx", "base", VERTEX_SHADER);
	game->LoadShader("Resources/Shaders/PixelShader.fx", "lit", PIXEL_SHADER);
	game->LoadShader("Resources/Shaders/CelShader.fx", "cel", PIXEL_SHADER);
}

void LoadMeshes()
{
	Print();
	Print("Loading Meshes");

	game->LoadMesh("cube", "Resources/Meshes/cube.obj");
	game->LoadMesh("skybox", "Resources/Meshes/inverse sphere.obj");
	game->LoadMesh("arenadoor", "Resources/Meshes/Arena/Map-Door.obj");
	game->LoadMesh("arenatop", "Resources/Meshes/Arena/Map-Door-Top.obj");
	game->LoadMesh("arenarock1", "Resources/Meshes/Arena/Map-Rock1.obj");
	game->LoadMesh("arenarock2", "Resources/Meshes/Arena/Map-Rock2.obj");
	game->LoadMesh("teddy", "Resources/Meshes/teddy.obj");
}

void LoadTextures()
{
	Print();
	Print("Loading Textures");

	game->LoadTexture("ice cube", "Resources/Textures/Ice Cube.tga");
	game->LoadTexture("ice floor", "Resources/Textures/Ice Floor.tga");
	game->LoadTexture("sky", "Resources/Textures/ice cube.tga");
	game->LoadTexture("cliff", "Resources/Textures/cliffs.tga");
	game->LoadTexture("teddy", "Resources/Textures/teddy.tga");
}

void LoadEntities()
{
	Print();
	Print("Loading Entities");

#pragma region North Wall
	game->LoadEntity("north1", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north1")->PositionSet(-12.5, 0.0f, 10.0f);
	game->GetEntity("north1")->RotationSet(0.0f, 45.0f, 0.0f);
	game->AssignTexture("north1", "cliff");

	game->LoadEntity("north2", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north2")->PositionSet(-10.0f, 0.0f, 10.0f);
	game->GetEntity("north2")->RotationSet(0.0f, 100.0f, 0.0f);
	game->AssignTexture("north2", "cliff");

	game->LoadEntity("north3", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north3")->PositionSet(-7.5f, 0.0f, 10.0f);
	game->GetEntity("north3")->RotationSet(0.0f, 90.0f, 0.0f);
	game->AssignTexture("north3", "cliff");

	game->LoadEntity("north4", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north4")->PositionSet(-5.0f, 0.0f, 10.0f);
	game->GetEntity("north4")->RotationSet(0.0f, 95.0f, 0.0f);
	game->AssignTexture("north4", "cliff");

	game->LoadEntity("north5", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north5")->PositionSet(-2.5f, 0.0f, 10.0f);
	game->GetEntity("north5")->RotationSet(0.0f, 80.0f, 0.0f);
	game->AssignTexture("north5", "cliff");

	game->LoadEntity("north6", "arenadoor", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north6")->PositionSet(0.0f, 0.0f, 10.0f);
	game->GetEntity("north6")->RotationSet(0.0f, 90.0f, 0.0f);
	game->AssignTexture("north6", "cliff");

	game->LoadEntity("north6top", "arenatop", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north6top")->PositionSet(0.0f, 0.25f, 10.0f);
	game->GetEntity("north6top")->RotationSet(0.0f, 90.0f, 0.0f);
	game->AssignTexture("north6top", "cliff");

	game->LoadEntity("north7", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north7")->PositionSet(2.3f, 0.0f, 10.0f);
	game->GetEntity("north7")->RotationSet(0.0f, 85.0f, 0.0f);
	game->AssignTexture("north7", "cliff");

	game->LoadEntity("north8", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north8")->PositionSet(4.6f, 0.0f, 10.0f);
	game->GetEntity("north8")->RotationSet(0.0f, 105.0f, 0.0f);
	game->AssignTexture("north8", "cliff");

	game->LoadEntity("north9", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north9")->PositionSet(6.9f, 0.0f, 10.0f);
	game->GetEntity("north9")->RotationSet(0.0f, 82.0f, 0.0f);
	game->AssignTexture("north9", "cliff");

	game->LoadEntity("north10", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north10")->PositionSet(9.2f, 0.0f, 10.0f);
	game->GetEntity("north10")->RotationSet(0.0f, 90.0f, 0.0f);
	game->AssignTexture("north10", "cliff");

	game->LoadEntity("north11", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("north11")->PositionSet(11.5f, 0.0f, 10.0f);
	game->GetEntity("north11")->RotationSet(0.0f, 45.0f, 0.0f);
	game->AssignTexture("north11", "cliff");
#pragma endregion
#pragma region South Wall
	game->LoadEntity("south1", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south1")->PositionSet(-12.5, 0.0f, -10.0f);
	game->GetEntity("south1")->RotationSet(0.0f, -45.0f, 0.0f);
	game->AssignTexture("south1", "cliff");

	game->LoadEntity("south2", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south2")->PositionSet(-10.0f, 0.0f, -10.0f);
	game->GetEntity("south2")->RotationSet(0.0f, -100.0f, 0.0f);
	game->AssignTexture("south2", "cliff");

	game->LoadEntity("south3", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south3")->PositionSet(-7.5f, 0.0f, -10.0f);
	game->GetEntity("south3")->RotationSet(0.0f, -90.0f, 0.0f);
	game->AssignTexture("south3", "cliff");

	game->LoadEntity("south4", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south4")->PositionSet(-5.0f, 0.0f, -10.0f);
	game->GetEntity("south4")->RotationSet(0.0f, -95.0f, 0.0f);
	game->AssignTexture("south4", "cliff");

	game->LoadEntity("south5", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south5")->PositionSet(-2.5f, 0.0f, -10.0f);
	game->GetEntity("south5")->RotationSet(0.0f, -80.0f, 0.0f);
	game->AssignTexture("south5", "cliff");

	game->LoadEntity("south6", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south6")->PositionSet(0.0f, 0.0f, -10.0f);
	game->GetEntity("south6")->RotationSet(0.0f, -90.0f, 0.0f);
	game->AssignTexture("south6", "cliff");

	game->LoadEntity("south7", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south7")->PositionSet(2.3f, 0.0f, -10.0f);
	game->GetEntity("south7")->RotationSet(0.0f, -85.0f, 0.0f);
	game->AssignTexture("south7", "cliff");

	game->LoadEntity("south8", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south8")->PositionSet(4.6f, 0.0f, -10.0f);
	game->GetEntity("south8")->RotationSet(0.0f, -105.0f, 0.0f);
	game->AssignTexture("south8", "cliff");

	game->LoadEntity("south9", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south9")->PositionSet(6.9f, 0.0f, -10.0f);
	game->GetEntity("south9")->RotationSet(0.0f, -82.0f, 0.0f);
	game->AssignTexture("south9", "cliff");

	game->LoadEntity("south10", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south10")->PositionSet(9.2f, 0.0f, -10.0f);
	game->GetEntity("south10")->RotationSet(0.0f, -90.0f, 0.0f);
	game->AssignTexture("south10", "cliff");

	game->LoadEntity("south11", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("south11")->PositionSet(11.5f, 0.0f, -10.0f);
	game->GetEntity("south11")->RotationSet(0.0f, -45.0f, 0.0f);
	game->AssignTexture("south11", "cliff");
#pragma endregion
#pragma region East Wall
	game->LoadEntity("east1", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east1")->PositionSet(10.0f, 0.0f, 10.0f);
	game->GetEntity("east1")->RotationSet(0.0f, 190.0f, 0.0f);
	game->AssignTexture("east1", "cliff");

	game->LoadEntity("east2", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east2")->PositionSet(10.0f, 0.0f, 7.5f);
	game->GetEntity("east2")->RotationSet(0.0f, 180.0f, 0.0f);
	game->AssignTexture("east2", "cliff");

	game->LoadEntity("east3", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east3")->PositionSet(10.0f, 0.0f, 5.0f);
	game->GetEntity("east3")->RotationSet(0.0f, 185.0f, 0.0f);
	game->AssignTexture("east3", "cliff");

	game->LoadEntity("east4", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east4")->PositionSet(10.0f, 0.0f, 2.5f);
	game->GetEntity("east4")->RotationSet(0.0f, 170.0f, 0.0f);
	game->AssignTexture("east4", "cliff");

	game->LoadEntity("east5", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east5")->PositionSet(10.0f, 0.0f, 0.0f);
	game->GetEntity("east5")->RotationSet(0.0f, 180.0f, 0.0f);
	game->AssignTexture("east5", "cliff");

	game->LoadEntity("east6", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east6")->PositionSet(10.0f, 0.0f, -2.5f);
	game->GetEntity("east6")->RotationSet(0.0f, 175.0f, 0.0f);
	game->AssignTexture("east6", "cliff");

	game->LoadEntity("east7", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east7")->PositionSet(10.0f, 0.0f, -5.0f);
	game->GetEntity("east7")->RotationSet(0.0f, 195.0f, 0.0f);
	game->AssignTexture("east7", "cliff");

	game->LoadEntity("east8", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east8")->PositionSet(10.0f, 0.0f, -7.5f);
	game->GetEntity("east8")->RotationSet(0.0f, 172.0f, 0.0f);
	game->AssignTexture("east8", "cliff");

	game->LoadEntity("east9", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("east9")->PositionSet(10.0f, 0.0f, -10.0f);
	game->GetEntity("east9")->RotationSet(0.0f, 0.0f, 0.0f);
	game->AssignTexture("east9", "cliff");
#pragma endregion
#pragma region West Wall
	game->LoadEntity("west1", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west1")->PositionSet(-10.0f, 0.0f, 10.0f);
	game->GetEntity("west1")->RotationSet(0.0f, 10.0f, 0.0f);
	game->AssignTexture("west1", "cliff");

	game->LoadEntity("west2", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west2")->PositionSet(-10.0f, 0.0f, 7.5f);
	game->GetEntity("west2")->RotationSet(0.0f, 0.0f, 0.0f);
	game->AssignTexture("west2", "cliff");

	game->LoadEntity("west3", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west3")->PositionSet(-10.0f, 0.0f, 5.0f);
	game->GetEntity("west3")->RotationSet(0.0f, 5.0f, 0.0f);
	game->AssignTexture("west3", "cliff");

	game->LoadEntity("west4", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west4")->PositionSet(-10.0f, 0.0f, 2.5f);
	game->GetEntity("west4")->RotationSet(0.0f, -10.0f, 0.0f);
	game->AssignTexture("west4", "cliff");

	game->LoadEntity("west5", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west5")->PositionSet(-10.0f, 0.0f, 0.0f);
	game->GetEntity("west5")->RotationSet(0.0f, 0.0f, 0.0f);
	game->AssignTexture("west5", "cliff");

	game->LoadEntity("west6", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west6")->PositionSet(-10.0f, 0.0f, -2.5f);
	game->GetEntity("west6")->RotationSet(0.0f, -5.0f, 0.0f);
	game->AssignTexture("west6", "cliff");

	game->LoadEntity("west7", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west7")->PositionSet(-10.0f, 0.0f, -5.0f);
	game->GetEntity("west7")->RotationSet(0.0f, 15.0f, 0.0f);
	game->AssignTexture("west7", "cliff");

	game->LoadEntity("west8", "arenarock1", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west8")->PositionSet(-10.0f, 0.0f, -7.5f);
	game->GetEntity("west8")->RotationSet(0.0f, -8.0f, 0.0f);
	game->AssignTexture("west8", "cliff");

	game->LoadEntity("west9", "arenarock2", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west9")->PositionSet(-10.0f, 0.0f, -10.0f);
	game->GetEntity("west9")->RotationSet(0.0f, 0.0f, 0.0f);
	game->AssignTexture("west9", "cliff");

	game->LoadEntity("west91", "teddy", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("west91")->PositionSet(2.0f, 2.0f, 2.0f);
	game->GetEntity("west91")->RotationSet(0.0f, -170.0f, 0.0f);
	game->GetEntity("west91")->ScaleSet(0.1f, 0.1f, 0.1f);
	game->AssignTexture("west91", "teddy");
#pragma endregion
	game->LoadEntity("floor", "cube", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("floor")->ScaleSet(20.0f, 0.0f, 20.0f);
	game->AssignTexture("floor", "ice floor");

	game->LoadEntity("skybox", "skybox", "base", NULL, NULL, NULL, "cel");
	game->GetEntity("skybox")->RotationSet(0.0f, 0.0f, 180.0f);
	game->GetEntity("skybox")->ScaleSet(40.0f, 40.0f, 40.0f);
	game->AssignTexture("skybox", "sky");
}

void AttachConsole()
{
	AllocConsole();
	AttachConsole(GetCurrentProcessId());
	freopen("CON", "w", stdout);
}

GameState RunFrame(MSG* msg)
{
	if (msg != NULL) HandleInput(msg);
	return gameState;
}

bool btnParams[6] = { false, false, false, false, false, false };
bool released = true;
void HandleInput(MSG* msg)
{
	if (msg->wParam == VK_ESCAPE)
	{
		gameState = Terminate;
	}
	if (msg->wParam == 'W')
	{
		btnParams[0] = (msg->message == WM_KEYDOWN ? true : false);
	}
	else if (msg->wParam == 'S')
	{
		btnParams[1] = (msg->message == WM_KEYDOWN ? true : false);
	}
	else if (msg->wParam == 'A')
	{
		btnParams[2] = (msg->message == WM_KEYDOWN ? true : false);
	}
	else if (msg->wParam == 'D')
	{
		btnParams[3] = (msg->message == WM_KEYDOWN ? true : false);
	}
}


int x, y;
int teddyCount;
void RunParamaters()
{
	if (btnParams[0])
	{

	}
	if (btnParams[1])
	{

	}
	if (btnParams[2])
	{

	}
	if (btnParams[3])
	{

	}
	if (btnParams[4])
	{

	}
}

void HandleCursorPosition()
{
	float sensitivity = 1.0f;
	POINT point;
	GetCursorPos(&point);

	int width, height;
	GetResolution(&width, &height);
	float offsetX = point.x - (width / 2);
	float offsetY = point.y - (height / 2);

	int xScalar = 1;// offsetX > offsetY ? 1 : 2;
	int yScalar = 1;// offsetX > offsetY ? 2 : 1;

	if (abs(offsetX) > 1 * xScalar)
	{
		offsetX = abs(offsetX) / offsetX; //Gets a positive or negative multiplier;
	}

	if (abs(offsetY) > 1 * yScalar)
	{
		offsetY = abs(offsetY) / offsetY; //Gets a positive or negative multiplier;

	}
	SetCursorPos(width / 2, height / 2);
}

void GetResolution(int* width, int* height)
{
	RECT screen;

	const HWND hScreen = GetDesktopWindow();
	GetWindowRect(hScreen, &screen);
	*width = screen.right;
	*height = screen.bottom;
}