#pragma once
#include "Vector3.h"
class Transform
{
public:
	Transform();
	~Transform();

	Vector3 Position;
	Vector3 Rotation;
	Vector3 Scale;
};

