#include "PixelShader.h"

using namespace Shaders;

#pragma region Public Functions
//Constructors & Destructors

//Creates an empty pixel shader
PixelShader::PixelShader()
{
	shaderType = PIXEL_SHADER;
}

//Creates a new vertex shader
PixelShader::PixelShader(LPCSTR path, ID3D11Device* device, int ID, char* shaderTag)
{
	shaderID = ID;
	isShaderValid = CreateShader(path, device);
	shaderType = PIXEL_SHADER;
	tag = shaderTag;
}

//Cleans up the vertex shader
PixelShader::~PixelShader()
{
	//shader->Release();
	//layout->Release();
}

//Returns a boolean indicating the success rate of the shader's load sequence
bool PixelShader::GetInitializationState()
{
	return isShaderValid;
}

//Gets a pointer to the shader
ID3D11PixelShader* PixelShader::GetShader()
{
	return shader;
}

#pragma endregion
#pragma region Private Functions


//Creates a vertex shader from file
bool PixelShader::CreateShader(LPCSTR path, ID3D11Device* device)
{
	ID3DBlob* blob;
	HRESULT result;

	//Load the shader from file
	D3DX11CompileFromFile(path, 0, 0, "PixelShader_Main", "ps_4_0", 0, 0, 0, &blob, 0, &result);
	if (FAILED(result))
	{
		Functions::ThrowMessageBoxError("Error compiling pixel shader!", "[PixelShader]");
		return false;
	}

	//Creates the shader on the graphics card
	result = device->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &shader);
	if (FAILED(result))
	{
		Functions::ThrowMessageBoxError("Error creating pixel shader!", "[PixelShader]");
		return false;
	}

	blob->Release();

	return true;
}

#pragma endregion
