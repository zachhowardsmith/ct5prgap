#include "EntityManager.h"

#pragma region Public Functions
EntityManager::EntityManager()
{
}


EntityManager::~EntityManager()
{
}

//Creates an instance of an entity with the supplied creation paramaters
bool EntityManager::AddEntity(std::string tag, Mesh* mesh, VertexShader* vertexShader, HullShader* hullShader,
	DomainShader* domainShader, GeometryShader* geometryShader, PixelShader* pixelShader)
{
	entities[tag] = Entity();
	entities[tag].AssignMesh(mesh);

	if (vertexShader != NULL)
		entities[tag].AssignShader(vertexShader);

	if (hullShader != NULL)
		entities[tag].AssignShader(hullShader);

	if (domainShader != NULL)
		entities[tag].AssignShader(domainShader);

	if (geometryShader != NULL)
		entities[tag].AssignShader(geometryShader);

	if (pixelShader != NULL)
		entities[tag].AssignShader(pixelShader);


	Print("<%s> Entity Added", tag);
	return true;
}

Entity* EntityManager::operator[](LPCSTR tag)
{
	auto element = entities.find(tag);
	if (element != entities.end()) {
		return &element->second;
	}

	ThrowMessageBoxError("An entity was referenced that can not be found.", "[EntityManager]");
	return NULL;
}
#pragma endregion
#pragma region Private Functions
//Miscellaneous
#pragma endregion
