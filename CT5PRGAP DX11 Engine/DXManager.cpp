#include "DXManager.h"

DXManager::DXManager() { }
DXManager::DXManager(HWND hwnd) : hwnd(hwnd)
{
	initializationState = 
	(InitializeDeviceAndSwapChain() &&
	InitializeRasterizer() &&
	InitializeRenderTarget() &&
	InitializeDepthTexture() &&
	InitializeDepthStencilState() &&
	InitializeDepthStencilView() &&
	InitializeViewport() ? Enumerations::Initialize : Enumerations::Terminate);

	//OutputDebugString(static_cast<std::ostringstream*>(&(std::ostringstream() << "DXManager Initialization: " << (initializationState == Initialize ? "Succeeded\n" : "Failed\n")))->str().c_str());
}

#pragma region Initialization
//Initializes the device and swap chain
bool DXManager::InitializeDeviceAndSwapChain()
{
	Print("Initializing Device, Context and Swapchain");
	// create a struct to hold information about the swap chain
	DXGI_SWAP_CHAIN_DESC sc_Description;

	// clear out the struct for use
	ZeroMemory(&sc_Description, sizeof(DXGI_SWAP_CHAIN_DESC));

	// fill the swap chain description struct
	sc_Description.BufferCount = 1;                                    // one back buffer
	sc_Description.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;     // use 32-bit color
	sc_Description.BufferDesc.Width = Globals::Constants::SCREEN_WIDTH;         // set the back buffer width
	sc_Description.BufferDesc.Height = Globals::Constants::SCREEN_HEIGHT;       // set the back buffer height
	sc_Description.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;      // how swap chain is to be used
	sc_Description.OutputWindow = hwnd;                                // the window to be used
	sc_Description.SampleDesc.Count = 1;                               // how many multisamples
	sc_Description.SampleDesc.Quality = 0;                             // multisample quality level
	sc_Description.Windowed = true;                                    // windowed/full-screen mode
	sc_Description.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;     // allow full-screen switching

	// create a device, device context and swap chain using the information in the scd struct
	HRESULT result = D3D11CreateDeviceAndSwapChain(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
#ifdef _DEBUG
		D3D11_CREATE_DEVICE_DEBUG,
#else
		NULL,
#endif
		NULL,
		NULL,
		D3D11_SDK_VERSION,
		&sc_Description,
		&dx_swapChain,
		&dx_device,
		NULL,
		&dx_context);
	return Functions::CheckFail(&result);
}

//Initializes the rasterizer
bool DXManager::InitializeRasterizer()
{
	Print("Initializing Rasterizer");
	D3D11_RASTERIZER_DESC rasterDesc;

	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	ID3D11RasterizerState* rasterState;
	HRESULT result = dx_device->CreateRasterizerState(&rasterDesc, &rasterState);
	dx_context->RSSetState(rasterState);

	return Functions::CheckFail(&result);
}

//Initializes the render target
bool DXManager::InitializeRenderTarget()
{
	Print("Initializing Render Target");
	// get the address of the back buffer
	ID3D11Texture2D *pBackBuffer;
	dx_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

	// use the back buffer address to create the render target
	HRESULT result = dx_device->CreateRenderTargetView(pBackBuffer, NULL, &dx_backBuffer);
	pBackBuffer->Release();

	return Functions::CheckFail(&result);
}

//Initializes the Depth Texture
bool DXManager::InitializeDepthTexture()
{
	Print("Initializing Depth Texture");
	D3D11_TEXTURE2D_DESC depthTexDesc;
	ZeroMemory(&depthTexDesc, sizeof(depthTexDesc));
	depthTexDesc.Width = Globals::Constants::SCREEN_WIDTH;
	depthTexDesc.Height = Globals::Constants::SCREEN_HEIGHT;
	depthTexDesc.MipLevels = 1;
	depthTexDesc.ArraySize = 1;
	depthTexDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthTexDesc.SampleDesc.Count = 1;
	depthTexDesc.SampleDesc.Quality = 0;
	depthTexDesc.Usage = D3D11_USAGE_DEFAULT;
	depthTexDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthTexDesc.CPUAccessFlags = 0;
	depthTexDesc.MiscFlags = 0;

	HRESULT result = dx_device->CreateTexture2D(&depthTexDesc, NULL, &dx_depthTexture);
	return Functions::CheckFail(&result);
}

//Initializes the Depth Texture
bool DXManager::InitializeDepthStencilState()
{
	Print("Initializing Depth Stencil");
	D3D11_DEPTH_STENCIL_DESC depthDesc;
	ZeroMemory(&depthDesc, sizeof(depthDesc));
	depthDesc.DepthEnable = true;
	depthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthDesc.StencilEnable = true;
	depthDesc.StencilReadMask = 0xFF;
	depthDesc.StencilWriteMask = 0xFF;

	depthDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	depthDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	HRESULT result = dx_device->CreateDepthStencilState(&depthDesc, &dx_depthStencilState);
	dx_context->OMSetDepthStencilState(dx_depthStencilState, 1);

	return Functions::CheckFail(&result);
}

//Initializes the Depth Texture
bool DXManager::InitializeDepthStencilView()
{
	Print("Initializing Depth Stencil View");
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	HRESULT result = dx_device->CreateDepthStencilView(dx_depthTexture, &descDSV, &dx_depthStencilView);

	// set the render target as the back buffer
	dx_context->OMSetRenderTargets(1, &dx_backBuffer, dx_depthStencilView);

	return Functions::CheckFail(&result);
}

//Initializes the viewport
bool DXManager::InitializeViewport()
{
	Print("Initializing Viewport with resolution %ix%i", Globals::Constants::SCREEN_WIDTH, Globals::Constants::SCREEN_HEIGHT);
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = Globals::Constants::SCREEN_WIDTH;
	viewport.Height = Globals::Constants::SCREEN_HEIGHT;
	viewport.MaxDepth = 1.0f;

	dx_context->RSSetViewports(1, &viewport);

	return true;
}

//Terminates the object and cleans memory
void DXManager::Terminate()
{
	dx_swapChain->Release();
	Print("Releasing Swap Chain");
	dx_device->Release();
	Print("Releasing Device");
	dx_context->Release();
	Print("Releasing Device Context");
	dx_backBuffer->Release();
	Print("Releasing Render Target");
	dx_depthTexture->Release();
	Print("Releasing Depth Texture");
	dx_depthStencilState->Release();
	Print("Releasing Depth Stencil State");
	dx_depthStencilView->Release();
	Print("Releasing Depth Stencil View");
}
#pragma endregion

#pragma region Mutators
//Gets 
GameState DXManager::GetInitializationState()
{
	return initializationState;
}

//Gets the active DirectX device
ID3D11Device* DXManager::GetDevice()
{
	return dx_device;
}

//Gets the active DirectX device context
ID3D11DeviceContext* DXManager::GetContext()
{
	return dx_context;
}

//Gets the render view target
ID3D11RenderTargetView* DXManager::GetBackBufferTarget()
{
	return dx_backBuffer;
}

//Gets the depth stencil view
ID3D11DepthStencilView* DXManager::GetStencilView()
{
	return dx_depthStencilView;
}
#pragma endregion
#pragma region Functions
//Flips the buffers, presenting the next buffer on the swap chain
void DXManager::FlipBuffers()
{
	dx_swapChain->Present(0, 0);
}
#pragma endregion
#pragma region Miscellaneous

#pragma endregion