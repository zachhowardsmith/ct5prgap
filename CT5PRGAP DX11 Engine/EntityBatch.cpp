#include "EntityBatch.h"

#pragma region Public Functions
EntityBatch::EntityBatch(ShaderSignature signature)
{

}

EntityBatch::~EntityBatch()
{

}

void EntityBatch::AddEntity(Entity* entity)
{
	instances.insert(std::make_pair(entity->RetrieveMesh(), entity->GetInstance()));
}

Mesh* EntityBatch::GetEntityMesh()
{
	if (instances.size() > 0)
	{
		return instances.begin()->first;
	}
	else
	{
		return NULL;
	}
}

const std::multimap<Mesh*, Instance*> EntityBatch::GetInstanceBatch()
{
	return instances;
}

#pragma endregion
#pragma region Private Functions

#pragma endregion
