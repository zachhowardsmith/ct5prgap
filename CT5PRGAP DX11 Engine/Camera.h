#pragma once
#include "Vector3.h"
#include <xnamath.h>
class Camera
{
public:
	Camera();
	Camera(Vector3 position);
	Camera(Vector3 position, Vector3 rotation);
	~Camera();

	void FormMatrix();
	XMMATRIX GetMatrix();
	Vector3 GetLookDirection();

	void MoveTo(float x, float y, float z);
	void MoveTo(Vector3 position);
	void MoveBy(float x, float y, float z);
	void MoveBy(Vector3 offset);
	void RotateTo(float x, float y, float z);
	void RotateTo(Vector3 position);
	void RotateBy(float x, float y, float z);
	void RotateBy(Vector3 position);
	Vector3 Position();
	Vector3 Rotation();
private:
	Vector3 position;
	Vector3 rotation;
	Vector3 lookDirection;
	XMMATRIX matrix;
};

