#pragma once
#pragma region System Includes
#pragma endregion
#pragma region Custom Includes
#include "DXManager.h" 
#include <xnamath.h>
#pragma endregion

class Vertex
{
public:

#pragma region Public Functions
public:
	//Constructors & Destructors
	Vertex();
	Vertex(float x, float y, float z);
	Vertex(float x, float y, float z, float tx, float ty);
	~Vertex();


	//Functions
	Vertex Translate(Vertex offset);
#pragma endregion
#pragma region Private Functions
private:
	//Miscellaneous
#pragma endregion

#pragma region Public Variables
	XMFLOAT3 position;
	XMFLOAT2 tex0;
	XMFLOAT3 normal;
#pragma endregion
};