#pragma once
#pragma region Includes

#pragma region System
#include <map>
#pragma endregion

#pragma region Custom
#include "DXManager.h"
#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "GeometryShader.h"
#include "PixelShader.h"
#pragma endregion
#pragma endregion

using namespace Shaders;
using namespace Globals;
using namespace Enumerations;

class ShaderManager
{
public:
	ShaderManager::ShaderManager(DXManager* dx_mgr);
	ShaderManager::~ShaderManager();

	bool AddShader(LPCSTR file, char* tag, ShaderType type);

	VertexShader* GetVertexShader(LPCSTR tag);
	std::pair< bool, VertexShader* > GetVertexShaderSafe(LPCSTR tag);

	HullShader* GetHullShader(LPCSTR tag);
	std::pair< bool, HullShader* > GetHullShaderSafe(LPCSTR tag);

	DomainShader* GetDomainShader(LPCSTR tag);
	std::pair< bool, DomainShader* > GetDomainShaderSafe(LPCSTR tag);

	GeometryShader* GetGeometryShader(LPCSTR tag);
	std::pair< bool, GeometryShader* > GetGeometryShaderSafe(LPCSTR tag);

	PixelShader* GetPixelShader(LPCSTR tag);
	std::pair< bool, PixelShader* > GetPixelShaderSafe(LPCSTR tag);

	auto GetShader(char* tag, ShaderType type)->std::pair < bool, void* >;

	
private:
	DXManager* dx_manager;

	std::map<char*, VertexShader> vertexShader;
	std::map<char*, HullShader> hullShader;
	std::map<char*, DomainShader> domainShader;
	std::map<char*, GeometryShader> geometryShader;
	std::map<char*, PixelShader> pixelShader;

};

