#pragma once
#pragma region System Includes
#include <map>
#include <vector>
#pragma endregion
#pragma region Custom Includes
#include "ShaderSignature.h"
#include "Entity.h"
#include "Instance.h"
#include "Mesh.h"
#pragma endregion
class EntityBatch
{
#pragma region Public Functions
public:
	EntityBatch(ShaderSignature signature);
	~EntityBatch();

	void AddEntity(Entity* entity);
	Mesh* GetEntityMesh();
	
	const std::multimap<Mesh*, Instance*> GetInstanceBatch();
#pragma endregion
#pragma region Private Functions
private:

#pragma endregion

#pragma region Public Variables
public:

#pragma endregion
#pragma region Private Variables
private:
	std::multimap<Mesh*, Instance*> instances;

#pragma endregion
};

