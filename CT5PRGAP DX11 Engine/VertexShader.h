#pragma once
#pragma region System Includes
#pragma endregion
#pragma region Custom Includes
#include "Shader.h"
#pragma endregion

namespace Shaders
{
	class VertexShader : public Shader
	{

#pragma region Public Functions
	public:
		VertexShader();
		VertexShader(LPCSTR path, ID3D11Device* device, int ID, char* shaderTag);
		~VertexShader();

		bool GetInitializationState();

		ID3D11VertexShader* GetShader();
		ID3D11InputLayout* GetLayout();
#pragma endregion
#pragma region Private Functions
	private:
		bool CreateShader(LPCSTR path, ID3D11Device* device);
#pragma endregion

#pragma region Public Variables

#pragma endregion
#pragma region Private Variables
	private:
		bool isShaderValid;
		ID3D11VertexShader* shader;
		ID3D11InputLayout* layout;
#pragma endregion
	};
}