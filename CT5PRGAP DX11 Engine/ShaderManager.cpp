#include "ShaderManager.h"


ShaderManager::ShaderManager(DXManager* dx_mgr) : dx_manager(dx_mgr) { }

ShaderManager::~ShaderManager()
{
}

//Make it polymorphous by using a shader* pointer and creating a subtype through shader type. Simplifies the get call.
bool ShaderManager::AddShader(LPCSTR file, char* tag, Enumerations::ShaderType type)
{
	ID3DBlob *shader = 0;
	switch (type)
	{
		case VERTEX_SHADER:
		{
			Print("<%s> Creating Vertex Shader", tag);
			vertexShader[tag] = VertexShader(file, dx_manager->GetDevice(), vertexShader.size(), tag);
			if (!vertexShader[tag].GetInitializationState())
			{
				ThrowMessageBoxError("Error creating vertex shader!", "[ShaderManager]");
				vertexShader.erase(tag);
				return false;
			}
			return true;
			break;
		}
		case HULL_SHADER:
			Print("<%s> Creating Hull Shader", tag);
			break;
		case DOMAIN_SHADER:
			Print("<%s> Creating Domain Shader", tag);
			break;
		case GEOMETRY_SHADER:
			Print("<%s> Creating Geometry Shader", tag);
			break;
		case PIXEL_SHADER:
			Print("<%s> Creating Pixel Shader", tag);
			pixelShader[tag] = PixelShader(file, dx_manager->GetDevice(), pixelShader.size(), tag);
			if (!pixelShader[tag].GetInitializationState())
			{
				ThrowMessageBoxError("Error creating pixel shader!", "[ShaderManager]");
				pixelShader.erase(tag);
				return false;
			}
			return true;
			break;
	}
	return false;
}

//Attempts to return a tagged vertex shader
VertexShader* ShaderManager::GetVertexShader(LPCSTR tag)
{
	auto element = vertexShader.find((char*)tag);
	if (element != vertexShader.end()) {
		return &element->second;
	}
	return NULL;
}

//Attempts to return a tagged vertex shader
std::pair< bool, VertexShader* > ShaderManager::GetVertexShaderSafe(LPCSTR tag)
{
	auto element = vertexShader.find((char*)tag);
	if (element != vertexShader.end()) {
		return std::make_pair< bool, VertexShader* >(true, &(element->second));
	}
	return std::make_pair< bool, VertexShader* >(false, NULL);
}

//Attempts to return a tagged pixel shader
HullShader* ShaderManager::GetHullShader(LPCSTR tag) 
{
	return NULL;
}

//Attempts to return a tagged pixel shader
std::pair< bool, HullShader* > GetHullShaderSafe(LPCSTR tag) { return std::pair< bool, HullShader* >(false, NULL); }

//Attempts to return a tagged pixel shader
DomainShader* ShaderManager::GetDomainShader(LPCSTR tag) 
{
	return NULL;
}

//Attempts to return a tagged pixel shader
std::pair< bool, DomainShader* > GetDomainShaderSafe(LPCSTR tag) { return std::pair< bool, DomainShader* >(false, NULL); }

//Attempts to return a tagged pixel shader
GeometryShader* ShaderManager::GetGeometryShader(LPCSTR tag) 
{ 
	return NULL;
}

//Attempts to return a tagged pixel shader
std::pair< bool, GeometryShader* > GetGeometryShaderSafe(LPCSTR tag)  { return std::pair< bool, GeometryShader* >(false, NULL); }

//Attempts to return a tagged pixel shader
std::pair< bool, PixelShader* > ShaderManager::GetPixelShaderSafe(LPCSTR tag)
{
	auto element = pixelShader.find((char*)tag);
	if (element != pixelShader.end()) {
		return std::make_pair< bool, PixelShader* >(true, &(element->second));
	}
	return std::make_pair< bool, PixelShader* >(false, NULL);
}

//Attempts to return a tagged pixel shader
PixelShader* ShaderManager::GetPixelShader(LPCSTR tag)
{
	auto element = pixelShader.find((char*)tag);
	if (element != pixelShader.end()) {
		return &element->second;
	}
	return NULL;
}

auto ShaderManager::GetShader(char* tag, ShaderType type)->std::pair < bool, void* >
{
	switch (type)
	{
		case VERTEX_SHADER:
		{
			auto element = vertexShader.find(tag);
			if (element != vertexShader.end()) {
				return std::make_pair< bool, void* >(true, &(element->second));
			}
			else 
			{
				return std::make_pair< bool, void* >(false, NULL);
			}
			break;
		}
	case HULL_SHADER:
		break;
	case DOMAIN_SHADER:
		break;
	case GEOMETRY_SHADER:
		break;
	case PIXEL_SHADER:
		break;
	}
}