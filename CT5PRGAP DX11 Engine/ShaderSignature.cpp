#include "ShaderSignature.h"

#pragma region Public Functions
ShaderSignature::ShaderSignature()
{
	for (int i = 0; i < 5; i++)
	{
		shaderID[i] = -1;
		shaderTag[i] = "";
	}
}


ShaderSignature::~ShaderSignature()
{

}

//Sets the ID of a shader element
void ShaderSignature::SetID(Shader* shader)
{
	shaderID[shader->shaderType] = shader->GetShaderID();
	shaderTag[shader->shaderType] = shader->GetShaderTag();
}

char* ShaderSignature::GetTag(ShaderType shaderType)
{
	return shaderTag[shaderType];
}

bool ShaderSignature::operator==(ShaderSignature signature)
{
	bool success = true;
	
	//Bitwise checks to return whether two signatures are equivalent
	for (int i = 0; i < 5; i++)
		success &= (signature.shaderID[i] == shaderID[i]);
	return success;
}


#pragma endregion
#pragma region Private Functions

#pragma endregion
