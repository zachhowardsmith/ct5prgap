#pragma once
#include "Instance.h"
#pragma region System Includes

#pragma endregion
#pragma region Custom Includes
#include "Transform.h"
#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "GeometryShader.h"
#include "PixelShader.h"
#include "Mesh.h"
#include "ShaderSignature.h"
#include "Texture.h"
#pragma endregion

using namespace Shaders;

class Entity
{
public:

#pragma region Public Functions
public:
	Entity();
	~Entity();

	void PositionSet(Vector3 position);
	void PositionSet(float x, float y, float z);
	void PositionOffset(Vector3 offset);
	void PositionOffset(float x, float y, float z);
	Vector3 PositionGet();

	void RotationSet(Vector3 rotation);
	void RotationSet(float x, float y, float z);
	void RotationOffset(Vector3 rotation);
	void RotationOffset(float x, float y, float z);
	Vector3 RotationGet();

	void ScaleSet(float scale);
	void ScaleSet(Vector3 scale);
	void ScaleSet(float x, float y, float z);
	Vector3 ScaleGet();

	Instance* GetInstance();
	void UpdateInstance();

	void AssignShader(Shader* shader);
	void* RetrieveShader(ShaderType type);
	VertexShader* RetrieveVertexShader();
	HullShader* RetrieveHullShader();
	DomainShader* RetrieveDomainShader();
	GeometryShader* RetrieveGeometryShader();
	PixelShader* RetrievePixelShader();
	ShaderSignature GetSignature();


	void AssignTexture(Texture* entityTexture);
	Texture* RetrieveTexture();

	void AssignMesh(Mesh* mesh);
	Mesh* RetrieveMesh();
	ID3D11Buffer* RetrieveMeshBuffer(BufferType type);

#pragma endregion
#pragma region Private Functions
private:
	//Miscellaneous
	bool CheckFail(HRESULT *result);
#pragma endregion

#pragma region Public Variables

#pragma endregion
#pragma region Private Variables
private:
	Mesh* entityMesh;
	Transform transform;
	Shader* shaders[5];
	Texture* texture;

	Instance* instanceData;
	bool textureSet;
#pragma endregion
};

