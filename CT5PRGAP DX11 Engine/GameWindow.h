#pragma once
#pragma region System Includes
#include <windows.h>    // include the basic windows header file
#include <stdio.h>
#include <sstream>
#include <random>
#pragma endregion
#pragma region Custom Includes
#include "GameManager.h"
#pragma endregion

class GameWindow
{
public:

#pragma region Public Functions
public:
	GameWindow(void(*loadFunction) (), GameState(*loopFunction) (MSG*));
	~GameWindow();

	int WINAPI Construct(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow, GameManager** gamePtr);
	GameManager* GetGameHandle();
#pragma endregion
#pragma region Private Functions
private:
	static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	
#pragma endregion

#pragma region Public Variables

#pragma endregion
#pragma region Private Variables
private:
	GameManager* gameManager;
	GameState currentState = Initialize;
	//Contains the loop function allocated by the user.
	void (*loadCallback) ();
	GameState (*loopCallback) (MSG*);
#pragma endregion
};