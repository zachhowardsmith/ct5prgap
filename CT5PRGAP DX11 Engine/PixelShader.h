#pragma once
#pragma region System Includes
#pragma endregion
#pragma region Custom Includes
#include "Shader.h"
#pragma endregion

namespace Shaders
{
	class PixelShader : public Shader
	{
#pragma region Public Functions
	public:
		PixelShader();
		PixelShader(LPCSTR path, ID3D11Device* device, int ID, char* shaderTag);
		~PixelShader();


		bool GetInitializationState();

		ID3D11PixelShader* GetShader();
#pragma endregion
#pragma region Private Functions
	private:
		bool CreateShader(LPCSTR path, ID3D11Device* device);
#pragma endregion

#pragma region Public Variables

#pragma endregion
#pragma region Private Variables
	private:
		bool isShaderValid;
		ID3D11PixelShader* shader;
#pragma endregion
	};
}