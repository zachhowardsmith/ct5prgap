Texture2DArray textureMap : register(t0);
SamplerState textureSampler : register(s0);
cbuffer CameraDirection : register(b0)
{
	float4 cameraDirection;
};

cbuffer CameraPosition : register(b1)
{
	float4 cameraPosition;
};

struct PixelShaderInput
{
	float4 pixelPosition	: SV_POSITION;
	float4 worldPosition	: WORLDPOSITION;
	float3 pixelTexture		: TEXCOORD0;
	float3 pixelNormal		: NORMAL;
};

struct AmbientLight
{
	float3 colour;
	float intensity;
};

struct Light
{
	float3 position;
	float reach;

	float diffuseIntensity;
	float3 diffuseColour;

	float specularIntensity;
	float3 specularColour;
};

//Forward Declarations
float4 PixelShader_Main(PixelShaderInput pixelInput) : SV_TARGET;
void PixelShader_ConstructLights();
float3 RunLights(PixelShaderInput pixelInput);
float3 PixelShader_GetDiffuse(PixelShaderInput pixelInput, Light light);
float3 PixelShader_GetSpecular(PixelShaderInput pixelInput, Light light);

//Variable Declarations
static const bool LIGHTING = true;
static const int NUM_LIGHTS = 1;
static AmbientLight ambientLight;
static Light lights[NUM_LIGHTS];

float4 PixelShader_Main(PixelShaderInput pixelInput) : SV_TARGET
{
	//Build the lighting
	PixelShader_ConstructLights();

	//Run the light calculations
	float3 finalColour = RunLights(pixelInput);

	//Return the processed pixel
	return float4(finalColour, 1);
}

void PixelShader_ConstructLights()
{
	ambientLight.colour = float3(0.5f, 0.5f, 0.5f);
	ambientLight.intensity = 1.0f;

	if (LIGHTING)
	{
		for (int i = 0; i < NUM_LIGHTS; i++)
		{
			switch (i)
			{
				case 0:
					lights[0].position = float3(0.0f, 10.0f, 0.0f);
					lights[0].reach = 10;
					lights[0].diffuseIntensity = 0.7f;
					lights[0].diffuseColour = float3(1.0, 1.0, 1.0);
					lights[0].specularIntensity = 1;
					lights[0].specularColour = float3(0.1f, 0.1f, 0.25f);
					break;
				case 1:
					lights[1].position = float3(-5.0f, 0.0f, -2.0f);
					lights[1].reach = 10;
					lights[1].diffuseIntensity = 1;
					lights[1].diffuseColour = float3(0.003921568627451f, 0.1098039215686275f, 0.2156862745098039f);
					lights[1].specularIntensity = 1;
					lights[1].specularColour = float3(0.1f, 0.1f, 0.25f);
					break;
				case 2:
					lights[0].position = float3(0.0f, 5.0f, -3.0f);
					lights[0].reach = 10;
					lights[0].diffuseIntensity = 1;
					lights[0].diffuseColour = float3(0.003921568627451f, 0.1098039215686275f, 0.2156862745098039f);
					lights[0].specularIntensity = 1;
					lights[0].specularColour = float3(0.1f, 0.1f, 0.25f);
					break;
				case 3:
					lights[1].position = float3(-5.0f, 0.0f, -2.0f);
					lights[1].reach = 10;
					lights[1].diffuseIntensity = 1;
					lights[1].diffuseColour = float3(0.003921568627451f, 0.1098039215686275f, 0.2156862745098039f);
					lights[1].specularIntensity = 1;
					lights[1].specularColour = float3(0.1f, 0.1f, 0.25f);
					break;
			}
		}
	}
	
}

float3 RunLights(PixelShaderInput pixelInput)
{
	float4 pixelColour = textureMap.Sample(textureSampler, pixelInput.pixelTexture);

	float3 diffuseLight = float3(0, 0, 0);
	float3 specularLight = float3(0, 0, 0);

	if (LIGHTING)
	{
		for (int i = 0; i < NUM_LIGHTS; i++)
		{
			diffuseLight += PixelShader_GetDiffuse(pixelInput, lights[i]);
			specularLight += PixelShader_GetSpecular(pixelInput, lights[i]);
		}
	}
	return (pixelColour * ambientLight.intensity) + (pixelColour * diffuseLight) + specularLight;
}

float3 PixelShader_GetDiffuse(PixelShaderInput pixelInput, Light light)
{
	float3 lightVector = normalize(light.position - pixelInput.worldPosition);
	float diffuse = clamp(dot(pixelInput.pixelNormal, lightVector), 0.0f, 1.0f);
	return diffuse * light.diffuseColour * light.diffuseIntensity;
}

float3 PixelShader_GetSpecular(PixelShaderInput pixelInput, Light light)
{
	float4 camPos = float4(0, 0, 0, 1);
	float3 lightVector = normalize(light.position - pixelInput.worldPosition);
	float3 viewVector = normalize(camPos - pixelInput.worldPosition);
	float3 halfVec = normalize(lightVector + viewVector);

	float angle = dot(pixelInput.pixelNormal, halfVec);
	float saturation = saturate(angle);
	float specular = pow(saturation, 25);
	return specular * light.specularColour * clamp(light.specularIntensity, 0, 1);
}