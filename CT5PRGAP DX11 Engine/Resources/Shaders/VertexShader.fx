cbuffer ViewProjection : register(b0)
{
	matrix viewProjectionMatrix;
};

struct VertexShaderInput
{
	float4 vertexPosition	: POSITION;
	float2 vertexTexture	: TEXCOORD0;
	float3 vertexNormal		: NORMAL0;

	float4x4 instanceMatrix	: INSTANCEPOS;
	uint instanceTexture	: INSTANCETEX;
};

struct PixelShaderInput
{
	float4 pixelPosition	: SV_POSITION;
	float4 worldPosition	: WORLDPOSITION;
	float3 pixelTexture		: TEXCOORD0;
	float3 pixelNormal		: NORMAL;
};

//Forward Declarations
PixelShaderInput VertexShader_Main(VertexShaderInput vertexInput);

PixelShaderInput VertexShader_Main(VertexShaderInput vertexInput)
{
	float4 worldPosition, finalPosition;
	PixelShaderInput vertexOutput = (PixelShaderInput)0;

	//Transform the vertex according to the instance, view and projection matrices
	worldPosition = vertexInput.vertexPosition;
	worldPosition = mul(vertexInput.vertexPosition, vertexInput.instanceMatrix);
	finalPosition = mul(worldPosition, viewProjectionMatrix);
	
	//Assign values to the pixel input variable
	vertexOutput.pixelPosition = finalPosition;
	vertexOutput.worldPosition = worldPosition;
	vertexOutput.pixelTexture.x = vertexInput.vertexTexture.x;
	vertexOutput.pixelTexture.y = vertexInput.vertexTexture.y;
	vertexOutput.pixelTexture.z = vertexInput.instanceTexture;
	vertexOutput.pixelNormal = normalize(mul(vertexInput.vertexNormal, vertexInput.instanceMatrix));

	//Pass through to the Pixel Shader
	return vertexOutput;
}