Texture2DArray textureMap : register(t0);
SamplerState textureSampler : register(s0);

cbuffer CameraDirection : register(b0)
{
	float4 cameraDirection;
};

cbuffer CameraPosition : register(b1)
{
	float4 cameraPosition;
};

struct PixelShaderInput
{
	float4 pixelPosition	: SV_POSITION;
	float4 worldPosition	: WORLDPOSITION;
	float3 pixelTexture		: TEXCOORD0;
	float3 pixelNormal		: NORMAL;
};

//Forward Declarations
float4 PixelShader_Main(PixelShaderInput pixelInput) : SV_TARGET;
float3 RunLights(PixelShaderInput pixelInput);

//Variable Declarations
static const int BAND_COUNT = 3;

float4 PixelShader_Main(PixelShaderInput pixelInput) : SV_TARGET
{
	//Run the light calculations
	float3 finalColour = RunLights(pixelInput);

	//Return the processed pixel
	return float4(finalColour, 1);
}
float3 RunLights(PixelShaderInput pixelInput)
{
	float4 pixelColour = textureMap.Sample(textureSampler, pixelInput.pixelTexture);


	float3 lightVals[] = { float3(-1, 20, 1), float3(-1, 1, -1) };

	float finVal = 0.0f;
	float specVal = 0.0f;
	int celBands = 4;
	int specBands = 5;
	for (int i = 0; i < 2; i++)
	{
		//Calculate Cel Shader
		float val = dot(pixelInput.pixelNormal, normalize(lightVals[i]));
		val = clamp(val, 1.0 / (float)celBands, 1.0);
		val *= celBands;
		val -= val % 1;
		val /= celBands;
		float3 dir = normalize(float3(pixelInput.worldPosition.xyz) - float3(cameraPosition.xyz));
		if (abs(dot(dir, pixelInput.pixelNormal)) < 0.2f)
		{
			val = 0.0f;
		}
		finVal += val;

		float3 pixelToCam = float3(cameraPosition.xyz) - float3(pixelInput.worldPosition.xyz);
		float3 pixelToLight = float3(lightVals[i].xyz) - float3(pixelInput.worldPosition.xyz);

		float specDot = dot(pixelToCam, pixelToLight);
		float specIntensity = specDot / 2.0f;
		specVal += specIntensity * 0.01f;
	}
	
	specVal = clamp(specVal, 1.0f / (float)specBands, 1.0f);
	specVal *= specBands;
	specVal += specVal % 1;
	specVal /= specBands;

	

	return ((pixelColour * finVal) + (pixelColour * specVal * 0.2));








	float dotProdCam = dot(pixelInput.pixelNormal, cameraDirection * -1);

	float shadeVal = 0.0f;
	float increment = 1.0f / BAND_COUNT;
	bool borderVal = false;
	for (int i = 0; i < BAND_COUNT; i++)
	{
		float mult = increment * (i + 1);

		if (dotProdCam < 0.15f)
		{
			borderVal = true;
		}

		if (dotProdCam <= mult)
		{
			shadeVal = mult;
			break;
		}
	}

}