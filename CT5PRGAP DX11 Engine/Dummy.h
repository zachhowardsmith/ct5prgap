#pragma once
#pragma region System Includes

#pragma endregion
#pragma region Custom Includes
#include "DXManager.h"
#pragma endregion

class Dummy
{
public:

#pragma region Public Functions
public:
	Dummy();
	~Dummy();

#pragma endregion
#pragma region Private Functions
private:

#pragma endregion

#pragma region Public Variables

#pragma endregion
#pragma region Private Variables
private:

#pragma endregion
};