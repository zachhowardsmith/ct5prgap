#include "VertexShader.h"

using namespace Shaders;

#pragma region Public Functions
//Constructors & Destructors

//Creates an empty vertex shader
VertexShader::VertexShader() { shaderType = VERTEX_SHADER; }

//Creates a new vertex shader
VertexShader::VertexShader(LPCSTR path, ID3D11Device* device, int ID, char* shaderTag)
{
	shaderID = ID;
	isShaderValid = CreateShader(path, device);
	shaderType = VERTEX_SHADER;
	tag = shaderTag;
}

//Cleans up the vertex shader
VertexShader::~VertexShader()
{
	//CLEANUP CAUSES ERROR
	//shader->Release();
	//layout->Release();
}

//Returns a boolean indicating the success rate of the shader's load sequence
bool VertexShader::GetInitializationState()
{
	return isShaderValid;
}

//Gets a pointer to the shader
ID3D11VertexShader* VertexShader::GetShader()
{
	return shader;
}

//Gets a pointer to the input layout
ID3D11InputLayout* VertexShader::GetLayout()
{
	return layout;
}
#pragma endregion
#pragma region Private Functions


//Creates a vertex shader from file
bool VertexShader::CreateShader(LPCSTR path, ID3D11Device* device)
{
	ID3DBlob* blob;
	HRESULT result;

	//Load the shader from file
	D3DX11CompileFromFile(path, 0, 0, "VertexShader_Main", "vs_4_0", 0, 0, 0, &blob, 0, &result);
	if (FAILED(result))
	{
		Functions::ThrowMessageBoxError("Error compiling vertex shader!", "[VertexShader]");
		return false;
	}
	
	//Creates the shader on the graphics card
	result = device->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &shader);
	if (FAILED(result))
	{
		Functions::ThrowMessageBoxError("Error creating vertex shader!", "[VertexShader]"); 
		return false;
	}

	//Creates the input layout
	D3D11_INPUT_ELEMENT_DESC solidColorLayout[] =
	{
		//Vertex Buffer
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },

		//Instance buffer
		{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCEPOS", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "INSTANCETEX", 0, DXGI_FORMAT_R32_UINT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
	};

	unsigned int totalLayoutElements = ARRAYSIZE(solidColorLayout);
	result = device->CreateInputLayout(solidColorLayout, totalLayoutElements, blob->GetBufferPointer(), blob->GetBufferSize(), &layout);
	if (FAILED(result))
	{
		Functions::ThrowMessageBoxError("Error creating vertex shader!", "[VertexShader]");
		return false;
	}

	blob->Release();

	return true;
}

#pragma endregion
