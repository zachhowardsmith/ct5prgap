#pragma warning( disable : 4005)

#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define RAISE __FILE__ "("__STR1__(__LINE__)") : Warning Msg: "

#pragma once
#include <Windows.h>
#pragma region System
#include <d3d11.h>
#include <D3DX11.h>
#include <stdarg.h>

#include <sstream>
#pragma endregion

#pragma region Libraries
// include the Direct3D Library file
#pragma endregion

namespace Globals
{
	namespace TypeDefinitions
	{
		typedef unsigned int uint;
	}

	namespace Enumerations
	{
		//Specifies the current state of the game
		enum GameState
		{
			Initialize,
			Pause,
			Terminate
		};

		enum RenderBufferType
		{
			RENDER_BUFFER_VERTEX,
			RENDER_BUFFER_INDEX,
			RENDER_BUFFER_INSTANCE
		};

		//Specifies the types of buffers that can be defined and retrieved
		enum BufferType
		{
			VERTEX_BUFFER,
			INDEX_BUFFER
		};

		//Specifies the types of shaders the can be used
		enum ShaderType
		{
			VERTEX_SHADER = 0,
			TESSELATION_CONTROL_SHADER = 1,
			HULL_SHADER = 1,
			TESSELATION_EVALUATION_SHADER = 2,
			DOMAIN_SHADER = 2,
			GEOMETRY_SHADER = 3,
			FRAGMENT_SHADER = 4,
			PIXEL_SHADER = 4
		};
	}

	namespace Constants
	{
		const float MOVE_SPEED = 0.055f;	//The movement speed

		const int SCREEN_WIDTH = 800;		//The screen's width
		const int SCREEN_HEIGHT = 800;		//The screen's height

		//Mathematical Constants
		const float MATH_PI = 3.14159265358979323846f;
		const float MATH_PI_2 = 1.57079632679489661923f;
		const float MATH_PI_4 = 0.785398163397448309616f;
	}

	namespace Functions
	{
		static void Print();
		static void Print(char* str, ...);
		static bool CheckFail(HRESULT* result);
		static bool CheckFailWithError(HRESULT result, LPCSTR msg, LPCSTR sourceFile);
		static void ThrowMessageBoxError(LPCSTR msg, LPCSTR sourceFile);

		static void Print()
		{
			printf("\n");
		}

		static void Print(char* str, ...)
		{
			char buf[100];

			va_list argPtr;
			va_start(argPtr, str);
			vsprintf(buf, str, argPtr);
			va_end(argPtr);

			printf("%s%s.\n", "[DXENGINE] ", buf);
		}

		//Evaluates the fail state of a result
		static bool CheckFail(HRESULT* result)
		{
			return CheckFailWithError(*result, "An error occured.", "[Unspecified]");
		}

		//Checks the fail state and throws an error through a messagebox interface
		static bool CheckFailWithError(HRESULT result, LPCSTR msg, LPCSTR sourceFile)
		{
			if (FAILED(result))
			{
				Print("The previous operation failed.");
#ifdef _DEBUG
				MessageBox(0, msg, sourceFile, MB_OK);
#endif
				return false;
			}
			return true;
		}

		//Throws an error through a messagebox interface
		static void ThrowMessageBoxError(LPCSTR msg, LPCSTR sourceFile)
		{

#ifdef _DEBUG
			MessageBox(0, msg, sourceFile, MB_OK);
#endif
		}
	}
}

