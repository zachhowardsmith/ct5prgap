#pragma once
#pragma region System Includes
#include <math.h>
#pragma endregion
#pragma region Custom Includes
#include "DXManager.h"
#pragma endregion

using namespace Globals;

class Vector3
{
#pragma region Public Functions
public:
	//Constructors & Destructors
	Vector3();
	Vector3(float, float, float);
	~Vector3();

	//Functions
	void Normalize();
	Vector3 Normalized();
	void Rotate(float rotationX, float rotationY, float rotationZ);
	void Rotate(Vector3 rotation);
	float Magnitude();
	Vector3 GetPerpendicular();
	float X(bool radians = false);
	float Y(bool radians = false);
	float Z(bool radians = false);
	
	//Operater Overloads
	Vector3 operator+=(const Vector3 &rhs);
	Vector3 operator-=(const Vector3 &rhs);
	Vector3 Vector3::operator*=(const float &rhs);
	Vector3 Vector3::operator/=(const float &rhs);
	friend Vector3 operator+(Vector3 lhs, const Vector3 &rhs);
	friend Vector3 operator-(Vector3 lhs, const Vector3 &rhs);
	friend Vector3 operator*(Vector3 lhs, float rhs);
	friend Vector3 operator*(float lhs, Vector3 rhs);
	friend Vector3 operator/(Vector3 lhs, float rhs);
	friend Vector3 operator/(float lhs, Vector3 rhs);

#pragma endregion
#pragma region Private Functions
private:
	//Miscellaneous
	void RotateX(float rotation);
	void RotateY(float rotation);
	void RotateZ(float rotation);
	float DegreesToRadians(float degrees);
	float RadiansToDegrees(float degrees);
#pragma endregion

#pragma region Public Variables
	float x = 0;
	float y = 0;
	float z = 0;
#pragma endregion
#pragma region Private Variables
private:

#pragma endregion
};

