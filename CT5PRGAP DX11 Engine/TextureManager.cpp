#include "TextureManager.h"

#pragma region Public Functions
//Creates a new instance of a texture manager.
TextureManager::TextureManager(DXManager* dxPtr)
{
	dxManager = dxPtr;
	Print("Creating Texture Sampler");
	CreateSampler();
	MountSampler();
}

//Destroys the texture manager
TextureManager::~TextureManager()
{
	dxManager = NULL;
	samplerState->Release();
	textureMap.clear();
	texture3D->Release();
}

void TextureManager::SetResolution(int width, int height)
{
	texWidth = width;
	texHeight = height;
	outdated = true;
}

bool TextureManager::AddTexture(char* tag, LPCSTR fileName)
{
	Texture* tex = new Texture(fileName, textureMap.size());
	if (tex->GetInitializationState())
	{
		textureMap[tag] = tex;
		outdated = true;
		Print("<%s> Texture Loaded", tag);

		//Perform Check to see if texture size needs to be adjusted
		if (textureMap[tag]->Width() > texWidth)
			texWidth = textureMap[tag]->Width();

		if (textureMap[tag]->Height() > texHeight)
			texHeight = textureMap[tag]->Height();

		return true;
	}
	Print("<%s> Texture Load Failed", tag);
	return false;
}

//Creates a 3D texture and assigns it to the context.
void TextureManager::MountTexture3D()
{
	if (outdated)
	{
		CreateTexture3D();
		dxManager->GetContext()->PSSetShaderResources(0, 1, &texture3D);
		outdated = false;
	}
}

void TextureManager::MountSampler()
{
	dxManager->GetContext()->PSSetSamplers(0, 1, &samplerState);
}

//Operator overload allowing me to use the [] operator to access elements.
Texture* TextureManager::operator[](LPCSTR tag)
{
	auto element = textureMap.find(tag);
	if (element != textureMap.end()) 
	{
		return element->second;
	}
	ThrowMessageBoxError("A texture was referenced that can not be found.", "[TextureManager]");
	return NULL;
}

void TextureManager::CreateTexture3D()
{
	//Set up the 2D Texture Array descriptor
	D3D11_TEXTURE2D_DESC sTexDesc;
	sTexDesc.Width = texWidth;
	sTexDesc.Height = texHeight;
	sTexDesc.MipLevels = 1;
	sTexDesc.ArraySize = textureMap.size();
	sTexDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	sTexDesc.SampleDesc.Count = 1;
	sTexDesc.SampleDesc.Quality = 0;
	sTexDesc.Usage = D3D11_USAGE_DEFAULT;
	sTexDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	sTexDesc.CPUAccessFlags = 0;
	sTexDesc.MiscFlags = 0;

	//Initialize the Subresource Data ready for transfer to the GPU.
	D3D11_SUBRESOURCE_DATA* sSubData = new D3D11_SUBRESOURCE_DATA[textureMap.size()];

	for each(auto texture in textureMap)
	{
		texture.second->Resample(texWidth, texHeight);
		int id = texture.second->GetID();
		sSubData[id].pSysMem = texture.second->Image();
		sSubData[id].SysMemPitch = (UINT)(texWidth * 4);
		sSubData[id].SysMemSlicePitch = (UINT)(texWidth * texHeight * 4);
	}

	ID3D11Texture2D* pTexture;
	HRESULT res = dxManager->GetDevice()->CreateTexture2D(&sTexDesc, sSubData, &pTexture);

	D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;

	//Set the view Dimension
	viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	viewDesc.Texture2DArray.ArraySize = textureMap.size();
	viewDesc.Texture2DArray.FirstArraySlice = 0;
	viewDesc.Texture2DArray.MipLevels = 1;
	viewDesc.Texture2DArray.MostDetailedMip = 0;

	//Set the format
	viewDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;

	res = dxManager->GetDevice()->CreateShaderResourceView(pTexture, &viewDesc, &texture3D);

	pTexture->Release();
	delete[] sSubData;
	sSubData = NULL;
}

bool TextureManager::CreateSampler()
{
	D3D11_SAMPLER_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));
	textureDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	textureDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	textureDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	textureDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	textureDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	textureDesc.MaxLOD = D3D11_FLOAT32_MAX;
	HRESULT result = dxManager->GetDevice()->CreateSamplerState(&textureDesc,
		&samplerState);
	Globals::Functions::CheckFailWithError(result, "[FATAL] A sampler state could not be created and textures can therefore not be rendered.", "TextureManager");
	return SUCCEEDED(result);
}
#pragma endregion
