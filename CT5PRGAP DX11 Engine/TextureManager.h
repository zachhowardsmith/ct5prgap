#pragma once
#pragma region System Includes
#include <map>
#pragma endregion
#pragma region Custom Includes
#include "DXManager.h"
#include "Texture.h"
#pragma endregion

//Managerial class for textures.
class TextureManager
{
public:

#pragma region Public Functions
public:
	TextureManager(DXManager* dxPtr);
	~TextureManager();

	void SetResolution(int width, int height);
	bool AddTexture(char* tag, LPCSTR fileName);
	void MountTexture3D();
	void MountSampler();
	Texture* operator[](LPCSTR tag);
#pragma endregion
#pragma region Private Functions
private:
	void CreateTexture3D();
	bool CreateSampler();
#pragma endregion

#pragma region Public Variables
#pragma endregion
#pragma region Private Variables
	bool outdated = false; //Signifies whether the 3D texture needs to be rebuilt.
	int texWidth = 0;
	int texHeight = 0;
private:
	DXManager* dxManager;
	ID3D11SamplerState* samplerState;
	std::map<LPCSTR, Texture*> textureMap;
	ID3D11ShaderResourceView* texture3D;
#pragma endregion
};

