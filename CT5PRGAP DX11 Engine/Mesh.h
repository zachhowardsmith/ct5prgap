#pragma once
#pragma region System Includes
#include <fstream>
#include <vector>
#pragma endregion
#pragma region Custom Includes
#include "Vertex.h"
#include "DXManager.h"
#include "ErrorHandling.h"
#pragma endregion

using namespace Globals::Enumerations;
using namespace Error;
class Mesh
{

#pragma region Public Functions
public:
	Mesh();
	Mesh(LPCSTR filename, DXManager* dxPtr, int meshID);
	~Mesh();

	bool GetInitializationState();

	int GetMeshID();
	int GetIndexCount();
	ID3D11Buffer* GetBuffer(BufferType type);
#pragma endregion
#pragma region Private Functions
private:
	bool LoadMeshFromFile(LPCSTR fileName, int ID);
	bool BuildBuffer(Vertex* vertices, int vertexCount);
#pragma endregion

#pragma region Public Variables

#pragma endregion
#pragma region Private Variables
private:
	DXManager* dxPtr;
	int meshID = 0;
	bool isMeshValid;
	ID3D11Buffer* buffers[2];
	int indexCount;
#pragma endregion
};

