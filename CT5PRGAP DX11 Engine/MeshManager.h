#pragma once
#pragma region System Includes
#include <map>
#pragma endregion
#pragma region Custom Includes
#include "DXManager.h"
#include "Mesh.h"
#pragma endregion
class MeshManager
{

#pragma region Public Functions
public:
	MeshManager();
	MeshManager(DXManager* dxPtr);
	~MeshManager();

	bool LoadMesh(LPCSTR tag, LPCSTR filename);
	int GetMeshCount();

	Mesh* operator[](LPCSTR tag);

#pragma endregion
#pragma region Private Functions
private:
	//Miscellaneous
#pragma endregion

#pragma region Public Variables

#pragma endregion
#pragma region Private Variables
private:
	DXManager* dxManager;
	std::map<LPCSTR, Mesh> meshes;
#pragma endregion
};

