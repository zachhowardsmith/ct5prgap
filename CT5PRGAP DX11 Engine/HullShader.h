#pragma once
#include "Shader.h"
namespace Shaders
{
	class HullShader : public Shader
	{
	public:
		HullShader();
		~HullShader();
		ID3D11HullShader* GetShader();
	};
}