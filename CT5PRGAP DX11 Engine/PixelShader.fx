Texture2DArray textureMap : register(t0);
SamplerState textureSampler : register(s0);

cbuffer CameraPosition : register(b1)
{
	float4 cameraPosition;
};

struct PixelShaderInput
{
	float4 pixelPosition	: SV_POSITION;
	float4 worldPosition	: WORLDPOSITION;
	float3 pixelTexture		: TEXCOORD0;
	float4 pixelNormal		: NORMAL;
};

//Forward Declarations
float4 PixelShader_Main(PixelShaderInput pixelInput) : SV_TARGET;
float4 PixelShader_GetDiffuse(PixelShaderInput pixelInput, float3 lightVector);
float PixelShader_GetSpecular(PixelShaderInput pixelInput, float3 lightVector);

float4 PixelShader_Main(PixelShaderInput pixelInput) : SV_TARGET
{
	float4 pixelColour = textureMap.Sample(textureSampler, pixelInput.pixelTexture);

	float3 lightPosition = float3(2.0f, 0.0f, 0.0f);
	float3 lightVector = normalize(lightPosition - pixelInput.worldPosition);


	float4 diffuseLight = PixelShader_GetDiffuse(pixelInput, lightVector);
	//float4 specularLight = diffuseLight > 0 ? PixelShader_GetSpecular(pixelInput, lightVector) : 0;

	//Return the processed pixel
	return (diffuseLight * pixelColour);// +(specularLight * pixelColour);
}

float4 PixelShader_GetDiffuse(PixelShaderInput pixelInput, float3 lightVector)
{
	float diffuseColor = float4(1, 1, 0, 0);
	float diffuse = clamp(dot(pixelInput.pixelNormal, lightVector), 0.0f, 1.0f);
	float4 diffuseLight = diffuseColor * diffuse;
	return diffuseLight;
}


float PixelShader_GetSpecular(PixelShaderInput pixelInput, float3 lightVector)
{
	float3 viewVector = normalize(cameraPosition - pixelInput.worldPosition);
	float3 halfVec = normalize(lightVector + viewVector);

	float angle = dot(pixelInput.pixelNormal, halfVec);
	float saturation = saturate(angle);
	float specular = pow(saturation, 25);

	return specular;
}