#include "Camera.h"

Camera::Camera()
{
	position = Vector3(0, 0, 0);
	rotation = Vector3(0, 0, 0);
}

Camera::Camera(Vector3 position)
{
	this->position = position;
	rotation = Vector3(0, 0, 0);
}

Camera::Camera(Vector3 position, Vector3 rotation)
{
	this->position = position;
	this->rotation = rotation;
}

Camera::~Camera()
{
}

void Camera::FormMatrix()
{
	lookDirection = Vector3(0, 0, 1.0f);
	lookDirection.Rotate(rotation);

	XMFLOAT3 posVec(position.X(), position.Y(), position.Z());
	XMFLOAT3 rotVec(lookDirection.X(), lookDirection.Y(), lookDirection.Z());
	XMFLOAT3 upVec(0, 1, 0);

	matrix = XMMatrixTranspose(XMMatrixLookToLH(XMLoadFloat3(&posVec), XMLoadFloat3(&rotVec), XMLoadFloat3(&upVec)));
}

XMMATRIX Camera::GetMatrix()
{
	Vector3 lookDirection = Vector3(0, 0, 1.0f);
	lookDirection.Rotate(rotation);

	XMFLOAT3 posVec(position.X(), position.Y(), position.Z());
	XMFLOAT3 rotVec(lookDirection.X(), lookDirection.Y(), lookDirection.Z());
	XMFLOAT3 upVec(0, 1, 0);

	return XMMatrixTranspose(XMMatrixLookToLH(XMLoadFloat3(&posVec), XMLoadFloat3(&rotVec), XMLoadFloat3(&upVec)));
}

Vector3 Camera::GetLookDirection()
{
	return lookDirection;
}

void Camera::MoveTo(float x, float y, float z)
{
	MoveTo(Vector3(x, y, z));
}

void Camera::MoveTo(Vector3 position)
{
	this->position = position;
	FormMatrix();
}

void Camera::MoveBy(float x, float y, float z)
{
	MoveBy(Vector3(x, y, z));
}

void Camera::MoveBy(Vector3 offset)
{
	this->position -= offset;
	FormMatrix();
}

void Camera::RotateTo(float x, float y, float z)
{
	RotateTo(Vector3(x, y, z));
}

void Camera::RotateTo(Vector3 rotation)
{
	this->rotation = rotation;
	FormMatrix();
}

void Camera::RotateBy(float x, float y, float z)
{
	RotateBy(Vector3(x, y, z));
}

void Camera::RotateBy(Vector3 rotation)
{
	this->rotation += rotation;
	FormMatrix();
}

Vector3 Camera::Position()
{
	return position;
}

Vector3 Camera::Rotation()
{
	return rotation;
}