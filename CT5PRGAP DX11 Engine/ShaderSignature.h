#pragma once
#pragma region System Includes

#pragma endregion
#pragma region Custom Includes
#include "VertexShader.h"
#pragma endregion

using namespace Globals::Enumerations;

class ShaderSignature
{

#pragma region Public Functions
public:
	ShaderSignature();
	~ShaderSignature();

	void SetID(Shader* shader);
	char* GetTag(ShaderType shaderType);
	bool operator==(ShaderSignature signature);
#pragma endregion
#pragma region Private Functions
private:

#pragma endregion

#pragma region Public Variables
public:

#pragma endregion
#pragma region Private Variables
private:
	int shaderID[5];
	char* shaderTag[5];
#pragma endregion
};