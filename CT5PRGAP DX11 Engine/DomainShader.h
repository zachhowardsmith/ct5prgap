#pragma once
#include "Shader.h"
namespace Shaders
{
	class DomainShader : public Shader
	{
	public:
		DomainShader();
		~DomainShader();
		ID3D11DomainShader* GetShader();
	};
}

