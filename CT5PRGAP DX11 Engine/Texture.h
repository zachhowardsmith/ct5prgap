#pragma once
#pragma region System Includes
#include <fstream>
#pragma endregion
#pragma region Custom Includes
#include "DXManager.h"
#pragma endregion

class Texture
{
public:

#pragma region Public Functions
public:
	Texture(LPCSTR fileName, int ID);
	~Texture();

	bool GetInitializationState();
	void Resample(int width, int height);
	int GetID();

	int Width();
	int Height();
	byte* Image();
#pragma endregion
#pragma region Private Functions
private:
	bool LoadTGA(LPCSTR fileName);
	bool LoadTGABPP24(byte* bytes, int length);
	bool LoadTGABPP32(byte* bytes, int length);
	byte* Transpose(byte* data, int length);
	bool ResampleRequired(int width, int height);

#pragma endregion

#pragma region Public Variables

#pragma endregion
#pragma region Private Variables
	int id;
	bool initializationState;

	int width;
	int height;
	byte* imageData;
private:

#pragma endregion
};