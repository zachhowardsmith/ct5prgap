#include "Texture.h"

#pragma region Public Functions

//Creates a new instance of a texture resource.
Texture::Texture(LPCSTR fileName, int ID)
{
	initializationState = LoadTGA(fileName);
	id = ID;
}

//Destroys the texture resource.
Texture::~Texture()
{
	delete[] imageData;
	imageData = NULL;
}

//Gets a boolean indicating whether the texture resource has been successfully created.
bool Texture::GetInitializationState()
{
	return initializationState;
}

//Resizes the image so the 3D texture can be created containing this texture.
//Method found here: http://www.cplusplus.com/forum/general/2615/
void Texture::Resample(int destWidth, int destHeight)
{
	if (initializationState && ResampleRequired(destWidth, destHeight))
	{
		int memSize = destWidth * destHeight * 4;

		byte* newData = new byte[memSize];
		float multX = (float)destWidth / (float)width;
		float multY = (float)destHeight / (float)height;

		int stride = width * 4;
		for (int curY = 0; curY < destHeight; curY++)
		{
			for (int curX = 0; curX < destWidth; curX++)
			{
				//Finds a multiplier representing the distance down the Y of the image I am sampling.
				int posMultY = (int)(curY / multY) * stride;
				//Finds a multiplier representing the distance across the X of the image I am sampling.
				int posMultX = (int)(curX / multX) * 4;
				//Finalises the sample position
				int samplePos = posMultY + posMultX;
				//Finds the byte offset of the pixel I am currently trying to resample.
				int newPos = ((curY * destWidth) + curX) * 4;
				newPos = (curY * (destWidth * 4)) + (curX * 4);

				for (int i = 0; i < 4; i++)
				{
					newData[newPos + i] = imageData[samplePos + i];
				}
			}
		}

		delete[] imageData;

		imageData = new byte[memSize];
		memcpy(imageData, newData, memSize);

		delete[] newData;
		newData = NULL;

		width = destWidth;
		height = destHeight;
	}
}

//Gets the ID of the texture
int Texture::GetID()
{
	return id;
}
#pragma endregion
#pragma region Private Functions

bool Texture::LoadTGA(LPCSTR fileName)
{
	bool readStatus = false;

	std::fstream fileStream(fileName, std::ios::in | std::ios::binary);
	fileStream.seekg(0, fileStream.end);
	int length = fileStream.tellg();

	byte* bytes = new byte[length];

	fileStream.seekg(0, fileStream.beg);
	fileStream.read((char*)bytes, length);

	byte bpp = bytes[16];
	switch (bpp)
	{
	case 32:
		readStatus = LoadTGABPP32(bytes, length);
		break;
	default:
		ThrowMessageBoxError("The loaded Targa is not the compatible 32BPP required.  Please ensure all textures are output as 32BPP.", "[Texture]");
		readStatus = false;
		break;
	}

	delete[] bytes;
	bytes = NULL;

	return readStatus;
}

bool Texture::LoadTGABPP32(byte* bytes, int length)
{
	//Loads the width and the height.
	byte dimensionBits[4];
	for (int i = 12; i < 16; i++)
	{
		dimensionBits[i - 12] = bytes[i];
	}

	width = bytes[12] << 0 | bytes[13] << 8;
	height = bytes[14] << 0 | bytes[15] << 8;

	//width = dimensionBits[0] << 0 | dimensionBits[1] << 8;
	//height = dimensionBits[2] << 0 | dimensionBits[3] << 8;

	int startIndex = 18 + bytes[0]; //Incorporate the Colour Map Length. Unused at the moment.

	imageData = new byte[length - startIndex];
	memcpy(imageData, &bytes[startIndex], length - startIndex);
	imageData = Transpose(imageData, length - startIndex);
	return true;
}

//Transposes image data to fix the way TGAs write from the bottom left to top right.
byte* Texture::Transpose(byte* data, int length)
{
	byte* newData = new byte[/*width * height * 4*/ length];
	int stride = width * 4;
	for (int y = 0; y < height; y++)
	{
		int sampleSource = ((height - 1) - y) * stride;
		int sampleDest = y * stride;
		memcpy(&newData[sampleDest], &data[sampleSource], stride);
	}
	return newData;
}

//Checks to see whether the dimensions of the texture are as required.
bool Texture::ResampleRequired(int destWidth, int destHeight)
{
	return (width != destWidth) | (height != destHeight);
}

int Texture::Width()
{
	return width;
}

int Texture::Height()
{
	return height;
}

byte* Texture::Image()
{
	return imageData;
}

#pragma endregion
