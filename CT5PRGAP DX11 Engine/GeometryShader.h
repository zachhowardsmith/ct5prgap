#pragma once
#include "Shader.h"
namespace Shaders
{
	class GeometryShader : public Shader
	{
	public:
		GeometryShader();
		~GeometryShader();
		ID3D11GeometryShader* GetShader();
	};
}
