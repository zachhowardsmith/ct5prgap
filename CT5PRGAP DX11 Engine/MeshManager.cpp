#include "MeshManager.h"

#pragma region Public Functions
MeshManager::MeshManager()
{
}

MeshManager::MeshManager(DXManager* dxPtr)
{
	dxManager = dxPtr;
}

MeshManager::~MeshManager()
{
}

bool MeshManager::LoadMesh(LPCSTR tag, LPCSTR filename)
{
	meshes[tag] = Mesh(filename, dxManager, meshes.size());
	if (!meshes[tag].GetInitializationState())
	{
		ThrowMessageBoxError("Error creating mesh!", "[MeshManager]");
		meshes.erase(tag);
		Print("<%s> Mesh Load Failed", tag);
		return false;
	}
	Print("<%s> Mesh Loaded", tag);
	return true;
}

//Gets the amount of meshes that have currently been loaded in
int MeshManager::GetMeshCount()
{
	return meshes.size();
}

Mesh* MeshManager::operator[](LPCSTR tag)
{
	auto element = meshes.find(tag);
	if (element != meshes.end()) 
	{
		return &element->second;
	}

	ThrowMessageBoxError("A mesh was referenced that can not be found.", "[MeshManager]");
	return NULL;
}
#pragma endregion
#pragma region Private Functions
//Miscellaneous
#pragma endregion
