#pragma once
#pragma region System
#include <Windows.h>
#pragma endregion

#pragma region Custom
#include "Globals.h"
#pragma endregion

namespace Error
{
	enum ERROR_FLAGS
	{
		FLAGS_PRINT = 1 << 1,
		FLAGS_MSGBOX = 1 << 2
	};

	enum ERRORS
	{
		//Memory
		ERROR_MEMORY_ALLOCATION_FAILED,
		ERROR_MEMORY_ALLOCATION_ASSERT,

		//Input Output
		ERROR_IO_SEEK_FAIL,
		ERROR_IO_FILE_INVALID,

		//Mesh
		ERROR_MESH_CORRUPT,

		//Textures

		//Shaders
	};

	static void Throw(ERRORS error, ERROR_FLAGS flags);
	static void Throw(ERRORS error, LPCSTR file, ERROR_FLAGS flags);


	static void Throw(ERRORS error, ERROR_FLAGS flags = FLAGS_PRINT)
	{
		Throw(error, "", flags);
	}

	static void Throw(ERRORS error, LPCSTR file, ERROR_FLAGS flags = FLAGS_PRINT)
	{
		LPCSTR msg = "";
		switch (error)
		{
		case ERRORS::ERROR_MEMORY_ALLOCATION_FAILED:
			msg = "Memory allocation failed";
			break;
		case ERRORS::ERROR_MEMORY_ALLOCATION_ASSERT:
			msg = "ASSERT: Memory allocation failure";
			break;
		case ERRORS::ERROR_IO_SEEK_FAIL:
			msg = "IO stream seek operation failed";
			break;
		case ERRORS::ERROR_IO_FILE_INVALID:
			msg = "IO stream file is corrupt or non-existant";
			break;
		case ERRORS::ERROR_MESH_CORRUPT:
			msg = "Encountered corrupt mesh file.";
			break;
		}
		if (flags & FLAGS_PRINT)
			Print("<ERROR> %s", msg);

		if (flags & FLAGS_MSGBOX)
			ThrowMessageBoxError(msg, file);
	}
}

