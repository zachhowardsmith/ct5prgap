#include "Mesh.h"

#pragma region Public Functions
Mesh::Mesh() 
{

}

Mesh::Mesh(LPCSTR filename, DXManager* dxPtr, int meshID)
{
	this->dxPtr = dxPtr;
	isMeshValid = LoadMeshFromFile(filename, meshID);
}

Mesh::~Mesh()
{
}

bool Mesh::GetInitializationState()
{
	return isMeshValid;
}

bool Mesh::LoadMeshFromFile(LPCSTR fileName, int ID)
{
	std::fstream f(fileName, std::ios_base::in | std::ios::binary);
	if (f) //File is ready for reading
	{
		short vertexStride = sizeof(Vertex);					//Gets the stride per vertex
		long fileLength = f.seekg(0, std::ios::end).tellg();	//Gets the byte count
		float vertexCount = (float)fileLength / (float)vertexStride;			//Calculates the vertex count

		if (vertexCount == (int)vertexCount)					//Ensures there are no incomplete vertices
		{
			if (vertexCount > 134217727 || vertexCount * vertexStride > 2147483647)
			{
				//Alloc will definitely fail.
				Throw(ERROR_MEMORY_ALLOCATION_ASSERT);
			}
			else
			{
				char* vertices = new __nothrow char[fileLength];
				if (vertices == NULL)
				{
					//Allocation failed
					Throw(ERROR_MEMORY_ALLOCATION_FAILED);
				}
				else
				{
					//Read all vertices directly into the array and build the buffer
					if (f.seekg(0, std::ios::beg))
					{
						f.read(vertices, fileLength);
						for (int i = 0; i < fileLength; i++)
						{
							//1.6345 -0.0305 0.4379
							char b = vertices[i];
							if (i == fileLength - 32)
								int i2 = 12;
						}
						return BuildBuffer((Vertex*)vertices, (int)vertexCount);

						//std::vector<Vertex> debugData;
						//Vertex* iterator = reinterpret_cast<Vertex*>(vertices);
						//for (int i = 0; i < vertexCount; i++)
						//{
						//	debugData.push_back(iterator[i]);
						//}
					}
					else
					{
						Throw(ERROR_IO_SEEK_FAIL);
					}
				}
				delete[] vertices;
				vertices = NULL;
			}
		}
		else
		{
			Throw(ERROR_MESH_CORRUPT);
		}
	}
	else
	{
		Throw(ERROR_IO_FILE_INVALID);
	}
	return false;
}

bool Mesh::BuildBuffer(Vertex* vertices, int vertexCount)
{
	HRESULT result;

	D3D11_BUFFER_DESC vertexDesc;
	ZeroMemory(&vertexDesc, sizeof(vertexDesc));
	vertexDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexDesc.ByteWidth = sizeof(Vertex) * vertexCount;

	D3D11_SUBRESOURCE_DATA resourceData;
	ZeroMemory(&resourceData, sizeof(resourceData));
	resourceData.pSysMem = vertices;
	result = dxPtr->GetDevice()->CreateBuffer(&vertexDesc, &resourceData, &buffers[VERTEX_BUFFER]);

	Functions::CheckFailWithError(result, "An error occurred whilst creating a vertex buffer", "[Mesh]");

	WORD* indices = new WORD[vertexCount];

	for (int i = 0; i < vertexCount; i++)
	{
		indices[i] = i;
	}

	indexCount = vertexCount;

	D3D11_BUFFER_DESC indexDesc;
	ZeroMemory(&indexDesc, sizeof(indexDesc));
	indexDesc.Usage = D3D11_USAGE_DEFAULT;
	indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexDesc.ByteWidth = sizeof(WORD) * vertexCount;
	indexDesc.CPUAccessFlags = 0;

	resourceData.pSysMem = indices;

	result = dxPtr->GetDevice()->CreateBuffer(&indexDesc, &resourceData, &buffers[INDEX_BUFFER]);
	return CheckFailWithError(result, "An error occurred whilst creating an index buffer", "[Mesh]");
}

//Gets the ID of the mesh
int Mesh::GetMeshID()
{
	return meshID;
}

int Mesh::GetIndexCount()
{
	return indexCount;
}

//Gets one of multiple buffers that the mesh contains
ID3D11Buffer* Mesh::GetBuffer(BufferType type)
{
	return buffers[type];
}
//Initialization

//Mutators

#pragma endregion
#pragma region Private Functions
//Miscellaneous
#pragma endregion
