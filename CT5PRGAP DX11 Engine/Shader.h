#pragma once
#include <fstream>
#include "DXManager.h"

using namespace Globals::Enumerations;

class Shader
{
public:
	Shader();
	~Shader();


	ID3D11VertexShader* GetShader();

	void DebugError(ID3D10Blob* errorMessage, LPCSTR shaderFilename);
	int GetShaderID();
	char* GetShaderTag();

	ShaderType shaderType;
protected:
	int shaderID = -1;
	char* tag;
private:
	void OutputShaderErrorMessage(ID3D10Blob* errorMessage, LPCSTR shaderFilename);;
};

