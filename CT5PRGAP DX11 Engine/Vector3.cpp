#include "Vector3.h"

#pragma region Public Functions
#pragma region Constructors and Destructors
Vector3::Vector3() : x(0), y(0), z(0) { } 
Vector3::Vector3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) { }
Vector3::~Vector3() { }
#pragma endregion

#pragma region Functions
void Vector3::Normalize()
{
	float magnitude = Magnitude();
	x /= magnitude;
	y /= magnitude;
	z /= magnitude;
}

Vector3 Vector3::Normalized()
{
	float magnitude = Magnitude();
	return Vector3(x / magnitude, y / magnitude, z / magnitude);
}

void Vector3::Rotate(float rotationX, float rotationY, float rotationZ)
{
	RotateX(rotationX);
	RotateY(rotationY);
	RotateZ(rotationZ);
}

void Vector3::Rotate(Vector3 rotation)
{
	RotateX(rotation.x);
	RotateY(rotation.y);
	RotateZ(rotation.z);
}

float Vector3::Magnitude()
{
	return pow(pow(x, 2) + pow(y, 2) + pow(z, 2), 0.5f);
}

Vector3 Vector3::GetPerpendicular()
{
	return Vector3(-y, x, z);
}

float Vector3::X(bool radians)
{
	return radians ? DegreesToRadians(x) : x;
}

float Vector3::Y(bool radians)
{
	return radians ? DegreesToRadians(y) : y;
}

float Vector3::Z(bool radians)
{
	return radians ? DegreesToRadians(z) : z;
}
#pragma endregion

#pragma region Operator Overloads
Vector3 Vector3::operator+=(const Vector3 &rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	return *this;
}
Vector3 Vector3::operator-=(const Vector3 &rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	return *this;
}
Vector3 Vector3::operator*=(const float &rhs)
{
	x *= rhs;
	y *= rhs;
	z *= rhs;
	return *this;
}
Vector3 Vector3::operator/=(const float &rhs)
{
	x /= rhs;
	y /= rhs;
	z /= rhs;
	return *this;
}
Vector3 operator+(Vector3 lhs, const Vector3 &rhs)
{
	return Vector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
}
Vector3 operator-(Vector3 lhs, const Vector3 &rhs)
{
	return Vector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
}
Vector3 operator*(Vector3 lhs, float rhs)
{
	return Vector3(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
}
Vector3 operator*(float lhs, Vector3 rhs)
{
	return Vector3(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
}
Vector3 operator/(Vector3 lhs, float rhs)
{
	return Vector3(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs);
}
Vector3 operator/(float lhs, Vector3 rhs)
{
	return Vector3(lhs / rhs.x, lhs / rhs.y, lhs / rhs.z);
}
#pragma endregion

#pragma endregion
#pragma region Private Functions

void Vector3::RotateX(float rotation)
{
	float oldY = y;
	float oldZ = z;

	rotation = DegreesToRadians(rotation);
	y = oldY * cos(rotation) - oldZ * sin(rotation);
	z = oldY * sin(rotation) + oldZ * cos(rotation);
}

void Vector3::RotateY(float rotation)
{
	float oldX = x;
	float oldZ = z;

	rotation = DegreesToRadians(rotation);
	x = oldX * cos(rotation) + oldZ * sin(rotation);
	z = -oldX * sin(rotation) + oldZ * cos(rotation);
}

void Vector3::RotateZ(float rotation)
{
	float oldX= x;
	float oldY = y;

	rotation = DegreesToRadians(rotation);
	x = oldX * cos(rotation) - oldY * sin(rotation);
	y = oldX * sin(rotation) + oldY * cos(rotation);
}

float Vector3::DegreesToRadians(float degrees)
{
	return degrees * Constants::MATH_PI / 180.0f;
}

float Vector3::RadiansToDegrees(float radians)
{
	return radians / Constants::MATH_PI * 180.0f;
}
#pragma endregion
