#include "Vertex.h"

#pragma region Public Functions
//Constructors & Destructors
Vertex::Vertex() : position(XMFLOAT3(0, 0, 0)), tex0(XMFLOAT2(0, 0)) { }

Vertex::Vertex(float x, float y, float z) : position(XMFLOAT3(x, y, z)), tex0(XMFLOAT2(0, 0)) { }

Vertex::Vertex(float x, float y, float z, float tx, float ty) : position(XMFLOAT3(x, y, z)), tex0(XMFLOAT2(tx, ty)) { }

Vertex::~Vertex() { }

//Functions
Vertex Vertex::Translate(Vertex offset)
{
	return Vertex(position.x + offset.position.x, position.y + offset.position.y, position.z + offset.position.z);
}

//Mutators

#pragma endregion


