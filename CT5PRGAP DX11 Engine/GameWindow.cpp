#include "GameWindow.h"

#pragma region Public Functions
GameWindow::GameWindow(void(*loadFunction) (), GameState(*loopFunction) (MSG*))
{
	loadCallback = loadFunction;
	loopCallback = loopFunction;
}


GameWindow::~GameWindow()
{
}


//Constructs and initializes the window that will be used for rendering.
int WINAPI GameWindow::Construct(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow,
	GameManager** gamePtr)
{
	// the handle for the window, filled by a function
	HWND hWnd;
	// this struct holds information for the window class
	WNDCLASSEX wc;

	// clear out the window class for use
	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	// fill in the struct with the needed information
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	//wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName = "EngineDXClass1";

	// register the window class
	RegisterClassEx(&wc);

	RECT wr = { 0, 0, Globals::Constants::SCREEN_WIDTH, Globals::Constants::SCREEN_HEIGHT };    // set the size, but not the position
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);    // adjust the size
	srand(GetTickCount());
	// create the window and use the result as the handle
	hWnd = CreateWindowEx(NULL,
		"EngineDXClass1",    // name of the window class
		"DirectX Engine",   // title of the window
		WS_OVERLAPPEDWINDOW,    // window style
		0,    // x-position of the window
		0,    // y-position of the window
		Globals::Constants::SCREEN_WIDTH,    // width of the window
		Globals::Constants::SCREEN_HEIGHT,    // height of the window
		NULL,    // we have no parent window, NULL
		NULL,    // we aren't using menus, NULL
		hInstance,    // application handle
		NULL);    // used with multiple windows, NULL

	// display the window on the screen
	ShowWindow(hWnd, nCmdShow);
	Print("Created the GameWindow");

	// enter the main loop:

	// this struct holds Windows event messages
	MSG msg = { 0 };
	ShowCursor(false);
	SetCursorPos(Globals::Constants::SCREEN_WIDTH / 2, Globals::Constants::SCREEN_HEIGHT / 2);

	gameManager = new GameManager(hWnd);
	*gamePtr = gameManager;
	currentState = gameManager->GetInitializationState();
	loadCallback();
	Print("Ready to enter the game loop");

	int startTime = GetTickCount();
	int frameCounter = 0;

	// Enter the infinite message loop
	while (currentState != Terminate)
	{
		bool returnMessage = false;
		// Check to see if any messages are waiting in the queue
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// translate keystroke messages into the right format
			TranslateMessage(&msg);

			// send the message to the WindowProc function
			DispatchMessage(&msg);

			// check to see if it's time to quit
			if (msg.message == WM_QUIT)
			{
				currentState = Terminate;
				break;
			}
			else
			{
				returnMessage = true;
			}
		}

		currentState = loopCallback(returnMessage ? &msg : NULL);
		gameManager->Render();

		frameCounter++;
		int finishTime = GetTickCount();
		if (finishTime - startTime >= 1000)
		{
			Print("FPS: %i", frameCounter);
			frameCounter = 0;
			startTime = finishTime;
		}

		//OutputDebugString(static_cast<std::ostringstream*>(&(std::ostringstream() << "FPS: " << fps << "\n"))->str().c_str());
	}

	ShowCursor(true);
	// return this part of the WM_QUIT message to Windows
	return msg.wParam;
}


// this is the main message handler for the program
LRESULT CALLBACK GameWindow::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// sort through and find what code to run for the message given
	switch (message)
	{
		// this message is read when the window is closed
	case WM_DESTROY:
	{
		// close the application entirely
		PostQuitMessage(0);
		return 0;
	}
	break;
	}
	// Handle any messages the switch statement didn't
	return DefWindowProc(hWnd, message, wParam, lParam);
}

GameManager* GameWindow::GetGameHandle()
{
	return gameManager;
}

#pragma endregion
#pragma region Private Functions

#pragma endregion
