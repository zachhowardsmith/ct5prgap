#include "Entity.h"

#pragma region Public Functions
//Creates a new instance of an entity
Entity::Entity()
{
	instanceData = new Instance();
	textureSet = false;
	shaders[0] = new VertexShader();
	shaders[1] = new HullShader();
	shaders[2] = new DomainShader();
	shaders[3] = new GeometryShader();
	shaders[4] = new PixelShader();
	transform.Position = Vector3();
	transform.Rotation = Vector3();
	transform.Scale = Vector3(1, 1, 1);
}

//Cleans up the entity
Entity::~Entity()
{
	//Causes error
	//delete instanceData;
	//instanceData = NULL;
}

//Sets the position of the entity
void Entity::PositionSet(Vector3 position)
{
	transform.Position = position;
	UpdateInstance();
}

//Sets the position of the entity
void Entity::PositionSet(float x, float y, float z)
{
	transform.Position = Vector3(x, y, z);
	UpdateInstance();
}

//Moves the position of the entity by an offset
void Entity::PositionOffset(Vector3 offset)
{
	transform.Position += offset;
	UpdateInstance();
}

//Moves the position of the entity by an offset
void Entity::PositionOffset(float x, float y, float z)
{
	transform.Position += Vector3(x, y, z);
	UpdateInstance();
}

//Gets the position of the entity
Vector3 Entity::PositionGet()
{
	return transform.Position;
}

//Sets the rotation of the entity
void Entity::RotationSet(Vector3 rotation)
{
	transform.Rotation = rotation;
	UpdateInstance();
}

//Sets the rotation of the entity
void Entity::RotationSet(float x, float y, float z)
{
	transform.Rotation = Vector3(x, y, z);
	UpdateInstance();
}

//Changes the rotation of the entity by an offset
void Entity::RotationOffset(Vector3 rotation)
{
	transform.Rotation += rotation;
	UpdateInstance();
}

//Changes the rotation of the entity by an offset
void Entity::RotationOffset(float x, float y, float z)
{
	transform.Rotation += Vector3(x, y, z);
	UpdateInstance();
}

//Gets the rotation of the entity
Vector3 Entity::RotationGet()
{
	return transform.Rotation;
}

//Sets the scale of the entity
void Entity::ScaleSet(float scale)
{
	transform.Scale = Vector3(scale, scale, scale);
	UpdateInstance();
}

//Sets the scale of the entity
void Entity::ScaleSet(Vector3 scale)
{
	transform.Scale = scale;
	UpdateInstance();
}

//Sets the scale of the entity
void Entity::ScaleSet(float x, float y, float z)
{
	transform.Scale = Vector3(x, y, z); 
	UpdateInstance();
}

//Gets the scale of the entity
Vector3 Entity::ScaleGet()
{
	return transform.Scale;
}

//Gets a pointer to the instance data of the entity
Instance* Entity::GetInstance()
{
	return instanceData;
}

//Updates the instance data
void Entity::UpdateInstance()
{
	if (textureSet)
	{
		XMMATRIX translation = XMMatrixTranslation(transform.Position.X(), transform.Position.Y(), transform.Position.Z());
		XMMATRIX rotation = XMMatrixRotationRollPitchYaw(transform.Rotation.X(true), transform.Rotation.Y(true), transform.Rotation.Z(true));
		XMMATRIX scale = XMMatrixScaling(transform.Scale.X(), transform.Scale.Y(), transform.Scale.Z());

		instanceData->matrix = XMMatrixTranspose(scale * rotation * translation);
		instanceData->textureID = texture->GetID();
	}
}

//Assigns a shader to the entity [POLYMORPHOUS]
void Entity::AssignShader(Shader* shader)
{
	//Assigns the shader to the correct slot

	switch (shader->shaderType)
	{
	case VERTEX_SHADER:
		shaders[shader->shaderType] = (VertexShader*)shader;
		break;
	case HULL_SHADER:
		shaders[shader->shaderType] = (HullShader*)shader;
		break;
	case DOMAIN_SHADER:
		shaders[shader->shaderType] = (DomainShader*)shader;
		break;
	case GEOMETRY_SHADER:
		shaders[shader->shaderType] = (GeometryShader*)shader;
		break;
	case PIXEL_SHADER:
		shaders[shader->shaderType] = (PixelShader*)shader;
		break;
	}
}

//Gets the shader assigned to the entity
void* Entity::RetrieveShader(ShaderType type)
{
	switch (type)
	{
	case VERTEX_SHADER:
		return (VertexShader*)shaders[type];
		break;
	case HULL_SHADER:
		return (HullShader*)shaders[type];
		break;
	case DOMAIN_SHADER:
		return (DomainShader*)shaders[type];
		break;
	case GEOMETRY_SHADER:
		return (GeometryShader*)shaders[type];
		break;
	case PIXEL_SHADER:
		return (PixelShader*)shaders[type];
		break;
	}
	return shaders[type];
}

//Gets the vertex shader currently assigned to the entity
VertexShader* Entity::RetrieveVertexShader()
{
	return (VertexShader*)shaders[VERTEX_SHADER];
}

//Gets the hull shader currently assigned to the entity
HullShader* Entity::RetrieveHullShader()
{
	return (HullShader*)shaders[HULL_SHADER];
}

//Gets the domain shader currently assigned to the entity
DomainShader* Entity::RetrieveDomainShader()
{
	return (DomainShader*)shaders[DOMAIN_SHADER];
}

//Gets the geometry shader currently assigned to the entity
GeometryShader* Entity::RetrieveGeometryShader()
{
	return (GeometryShader*)shaders[GEOMETRY_SHADER];
}

//Gets the pixel shader currently assigned to the entity
PixelShader* Entity::RetrievePixelShader()
{
	return (PixelShader*)shaders[PIXEL_SHADER];
}

ShaderSignature Entity::GetSignature()
{
	ShaderSignature s;
	for (int i = 0; i < 5; i++)
		s.SetID(shaders[i]);
	return s;
}

//Assigns a texture to the entity
void Entity::AssignTexture(Texture* entityTexture)
{
	texture = entityTexture;
	textureSet = true;
	UpdateInstance();
}

//Gets the texture assigned to the entity
Texture* Entity::RetrieveTexture()
{
	return texture;
}

//Assigns a mesh to the entity
void Entity::AssignMesh(Mesh* mesh)
{
	entityMesh = mesh;
}

//Get the mesh assigned to the entity
Mesh* Entity::RetrieveMesh()
{
	return entityMesh;
}

//Get the buffer of the mesh assigned to the entity
ID3D11Buffer* Entity::RetrieveMeshBuffer(BufferType type)
{
	return entityMesh->GetBuffer(type);
}

#pragma endregion
#pragma region Private Functions
//Miscellaneous
#pragma endregion
