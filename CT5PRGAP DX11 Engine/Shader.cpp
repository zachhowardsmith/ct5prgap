#include "Shader.h"


Shader::Shader()
{
}


Shader::~Shader()
{
}

void Shader::DebugError(ID3D10Blob* errorMessage, LPCSTR shaderFilename)
{
	if (errorMessage)
		OutputShaderErrorMessage(errorMessage, (char*)shaderFilename);
	MessageBox(0, shaderFilename, "Missing Shader File", MB_OK);
}

int Shader::GetShaderID()
{
	return shaderID;
}

char* Shader::GetShaderTag()
{
	return tag;
}

void Shader::OutputShaderErrorMessage(ID3D10Blob* errorMessage, LPCSTR shaderFilename)
{
	char* compileErrors;
	unsigned long bufferSize, i;
	std::ofstream fout;


	// Get a pointer to the error message text buffer.
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	// Get the length of the message.
	bufferSize = errorMessage->GetBufferSize();

	// Open a file to write the error message to.
	fout.open("shader-error.txt");

	// Write out the error message.
	for (i = 0; i<bufferSize; i++)
	{
		fout << compileErrors[i];
	}

	// Close the file.
	fout.close();

	// Release the error message.
	errorMessage->Release();
	errorMessage = 0;

	// Pop a message up on the screen to notify the user to check the text file for compile errors.
	MessageBox(0, "Error compiling shader.  Check shader-error.txt for message.", shaderFilename, MB_OK);

	return;
}
