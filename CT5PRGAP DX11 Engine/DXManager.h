#pragma once
#pragma region Includes

#pragma region System
#include <sstream>
#pragma endregion

#pragma region Custom
#include "Globals.h"
#pragma endregion
#pragma endregion

using namespace Globals;
using namespace Globals::Functions;
using namespace Globals::Enumerations;
class DXManager
{
#pragma region Functions
#pragma region Public
public: //Functions

	DXManager();
	DXManager(HWND hwnd);

	//Initialization

	bool InitializeDeviceAndSwapChain();
	bool InitializeRasterizer();
	bool InitializeRenderTarget();
	bool InitializeDepthTexture();
	bool InitializeDepthStencilState();
	bool InitializeDepthStencilView();
	bool InitializeViewport();
	void Terminate();

	//Mutators

	GameState GetInitializationState();
	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetContext();
	ID3D11RenderTargetView* GetBackBufferTarget();
	ID3D11DepthStencilView* GetStencilView();
	ID3D11DepthStencilState* dx_depthStencilState;

	//Functions

	void FlipBuffers();
#pragma endregion
#pragma region Private
private:
	//Miscellaneous
#pragma endregion
#pragma endregion
#pragma region Variables
private:
	GameState initializationState; //Holds the state of the initialization. True for success. False for failure.
	HWND hwnd; //Handle to the window

	//Device and Swap chain
	IDXGISwapChain* dx_swapChain;
	ID3D11Device* dx_device;
	ID3D11DeviceContext* dx_context;

	//Render target and Viewport
	ID3D11RenderTargetView* dx_backBuffer;

	//Depth Details
	ID3D11DepthStencilView* dx_depthStencilView;
	ID3D11Texture2D* dx_depthTexture;
#pragma endregion
};

