#include "GameManager.h"

#pragma region Public Functions
//Constructors & Destructors

//Creates a new instance of a GameManager
GameManager::GameManager(HWND hwnd)
{
	Print("Initializing Game Manager");
	Print();

	Print("Initializing DXManager");
	dxManager = DXManager(hwnd);
	Print("DXManager Initialization ", (dxManager.GetInitializationState() == Initialize ? "Succeeded" : "Failed"));
	Print();

	Print("Initializing Mesh Manager");
	meshManager = MeshManager(&dxManager);
	Print();

	Print("Initializing Entity Manager");
	entityManager = EntityManager();
	Print();

	Print("Initializing Shader Manager");
	mgrShader = new ShaderManager(&dxManager);
	Print();

	Print("Initializing Texture Manager");
	textureManager = new TextureManager(&dxManager);
	Print();

	CreateConstantBuffer<XMMATRIX>(&viewProjectionCB);
	CreateConstantBuffer(16, &cameraPositionCB);
	CreateConstantBuffer(16, &cameraDirectionCB);

#pragma message(RAISE"Temporary memory alignment fix. Address later.")

	ALIGN XMMATRIX tmpMatrix = XMMatrixIdentity();
	tmpMatrix = XMMatrixPerspectiveFovLH(XM_PIDIV2, Constants::SCREEN_WIDTH / Constants::SCREEN_HEIGHT, 0.01f, 100.0f);
	projMatrix = XMMatrixTranspose(tmpMatrix);
}

GameManager::~GameManager()
{
}

template<typename T> void GameManager::CreateConstantBuffer(ID3D11Buffer** buffer)
{
	D3D11_BUFFER_DESC constDesc;
	ZeroMemory(&constDesc, sizeof(constDesc));
	constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constDesc.ByteWidth = sizeof(T);
	constDesc.Usage = D3D11_USAGE_DEFAULT;

	HRESULT result = dxManager.GetDevice()->CreateBuffer(&constDesc, 0, buffer);
	if (FAILED(result))
	{
		int i = 12;
	}
}

void GameManager::CreateConstantBuffer(int size, ID3D11Buffer** buffer)
{
	D3D11_BUFFER_DESC constDesc;
	ZeroMemory(&constDesc, sizeof(constDesc));
	constDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constDesc.ByteWidth = size;
	constDesc.Usage = D3D11_USAGE_DEFAULT;

	HRESULT result = dxManager.GetDevice()->CreateBuffer(&constDesc, 0, buffer);
	if (FAILED(result))
	{
		int i = 12;
	}
}
//Gets the initialization state of the DirectX manager
GameState GameManager::GetInitializationState()
{
	camera.RotateBy(45, 0.0f, 0);
	return dxManager.GetInitializationState();
}
int i = 0;
//Renders the current frame
void GameManager::Render()
{
	GenerateInstances();

	camera.MoveTo(0, 5, -8);
	camera.RotateBy(0, 0.01f, 0);


	if (dxManager.GetContext() == 0)
		return;

	float clearColor[4] = { 0.0f, 0.0f, 0.25f, 1.0f };
	dxManager.GetContext()->ClearRenderTargetView(dxManager.GetBackBufferTarget(), clearColor);
	dxManager.GetContext()->ClearDepthStencilView(dxManager.GetStencilView(),
		D3D11_CLEAR_DEPTH, 1.0f, 0);

	for (int buffer = 0; buffer < (int)renderBuffers[2].size(); buffer++)
	{
		uint stride[2] = { sizeof(Vertex), sizeof(Instance) };
		uint offset[2] = { 0, 0 };

		ShaderSignature currentSignature = uniqueSigs[buffer];

		dxManager.GetContext()->IASetInputLayout(mgrShader->GetVertexShader(currentSignature.GetTag(VERTEX_SHADER))->GetLayout());

		//VERTEX and INSTANCE DATA
		ID3D11Buffer* testBuffer[2] = { renderBuffers[RENDER_BUFFER_VERTEX][buffer], renderBuffers[RENDER_BUFFER_INSTANCE][buffer] };

		dxManager.GetContext()->IASetVertexBuffers(0, 2, testBuffer, stride, offset);
		dxManager.GetContext()->IASetIndexBuffer(renderBuffers[RENDER_BUFFER_INDEX][buffer], DXGI_FORMAT_R16_UINT, 0);
		dxManager.GetContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		dxManager.GetContext()->VSSetShader(mgrShader->GetVertexShader(currentSignature.GetTag(VERTEX_SHADER))->GetShader(), 0, 0);
		//dxManager.GetContext()->CSSetShader(mgrShader.GetComputeShader(currentSignature.GetTag(COMPUTE_SHADER))->GetShader(), 0, 0);
		//dxManager.GetContext()->HSSetShader(mgrShader->GetHullShader(currentSignature.GetTag(HULL_SHADER))->GetShader(), 0, 0);
		//dxManager.GetContext()->DSSetShader(mgrShader->GetDomainShader(currentSignature.GetTag(DOMAIN_SHADER))->GetShader(), 0, 0));
		//dxManager.GetContext()->GSSetShader(mgrShader->GetGeometryShader(currentSignature.GetTag(GEOMETRY_SHADER))->GetShader(), 0, 0);
		dxManager.GetContext()->PSSetShader(mgrShader->GetPixelShader(currentSignature.GetTag(PIXEL_SHADER))->GetShader(), 0, 0);

		//Textures
		textureManager->MountTexture3D();

		XMMATRIX concMatrix = XMMatrixMultiply(projMatrix, camera.GetMatrix());
		dxManager.GetContext()->UpdateSubresource(viewProjectionCB, 0, 0, &concMatrix, 0, 0);
		dxManager.GetContext()->VSSetConstantBuffers(0, 1, &viewProjectionCB);

		dxManager.GetContext()->UpdateSubresource(cameraDirectionCB, 0, 0, &camera.GetLookDirection(), 0, 0);
		dxManager.GetContext()->PSSetConstantBuffers(0, 1, &cameraDirectionCB);

		dxManager.GetContext()->UpdateSubresource(cameraPositionCB, 0, 0, &camera.Position(), 0, 0);
		dxManager.GetContext()->PSSetConstantBuffers(1, 1, &cameraPositionCB);

		dxManager.GetContext()->DrawIndexedInstanced(instanceIndexCounts[buffer], instanceCounts[buffer], 0, 0, 0);
	}
	dxManager.FlipBuffers();
}

//Starts the procedure to generate the instance buffers.
void GameManager::GenerateInstances()
{
	FlushBuffers();
	CompileInstanceBatches(buffersValid);
	buffersValid = true;
}

//Cleans the buffers from the previous pass
void GameManager::FlushBuffers()
{
	if (!buffersValid)
	{
		uniqueSigs.clear();
		uniqueBatches.clear();
		instanceCounts.clear();
		instanceIndexCounts.clear();

		for (int type = 0; type < 3; type++)
		{
			for (int buffer = 0; buffer < (int)renderBuffers[2].size(); buffer++)
			{
				renderBuffers[type][buffer]->Release(); //LEAK HERE. MUST BE COMMENTED OUT FOR GRAPHICS DEBUGGER TO WORK OTHERWISE IT CAUSES A CRASH
			}
		}
		renderBuffers[RENDER_BUFFER_VERTEX].clear();
		renderBuffers[RENDER_BUFFER_INDEX].clear();
		renderBuffers[RENDER_BUFFER_INSTANCE].clear();
	}
}

//Compiles a list of unique shader signatures from the entities.
void GameManager::CompileInstanceBatches(bool validBuffers)
{
	if (validBuffers)
	{
		UpdateInstanceBuffers(&uniqueBatches);
	}
	else
	{
		for (std::pair<std::string, Entity> entity : entityManager.entities)
		{
			ShaderSignature entitySig = entity.second.GetSignature();

			bool foundMatch = false;
			int index = 0;
			for (ShaderSignature sig : uniqueSigs)
			{
				if (sig == entitySig)
				{
					Mesh* mesh = uniqueBatches[index]->GetEntityMesh();
					if (mesh != NULL && entity.second.RetrieveMesh() == mesh)
					{
						foundMatch = true;
						uniqueBatches[index]->AddEntity(&entity.second);
					}

				}
				index++;
			}
			if (!foundMatch)
			{
				//Create a new batch and add the entity to it
				EntityBatch* newBatch = new EntityBatch(entitySig);
				newBatch->AddEntity(&entity.second);

				//Increment the index count list to include the index count for this mesh
				instanceIndexCounts.push_back(entity.second.RetrieveMesh()->GetIndexCount());

				//Add the signature to the list
				uniqueBatches.push_back(newBatch);
				uniqueSigs.push_back(entitySig);
			}
		}
		BuildInstanceBuffers(&uniqueBatches);
	}

}

//Constructs the instance buffers ready for uploading to the graphics card.
void GameManager::BuildInstanceBuffers(std::vector<EntityBatch*>* batches)
{
	for (EntityBatch* entity : *batches)
	{
		std::vector<Instance> instanceData;
		std::multimap<Mesh*, Instance*> currentBatch = entity->GetInstanceBatch();

		Mesh* currentMesh = currentBatch.begin()->first;
		renderBuffers[RENDER_BUFFER_INDEX].push_back(currentMesh->GetBuffer(INDEX_BUFFER));
		instanceCounts.push_back(0);

		for (auto iterator = currentBatch.begin(); iterator != currentBatch.end(); iterator++)
		{
			if (currentMesh == iterator->first)
			{
				//Same mesh, add to instance vector
				instanceData.push_back(*iterator->second);
				instanceCounts[instanceCounts.size() - 1]++;
			}
			else
			{
				renderBuffers[RENDER_BUFFER_INSTANCE].emplace_back();
				CreateInstanceBuffer(currentMesh, &instanceData);
				instanceData.clear();

				//Start of new mesh
				currentMesh = iterator->first;
				instanceData.push_back(*iterator->second);
				instanceCounts.push_back(0);
				instanceCounts[instanceCounts.size() - 1]++;

				renderBuffers[RENDER_BUFFER_INDEX].push_back(iterator->first->GetBuffer(INDEX_BUFFER));
			}
		}

		//renderBuffers[RENDER_BUFFER_INSTANCE].emplace_back();
		CreateInstanceBuffer(currentMesh, &instanceData);
		instanceData.clear();
	}
}

//Constructs the instance buffers ready for uploading to the graphics card.
void GameManager::UpdateInstanceBuffers(std::vector<EntityBatch*>* batches)
{
	//TODO
	//THIS FUNCTION MUST BE OPTIMISED AS THE ALLOCATION OF THE DATA ALONE IS SIGNIFICANTLY
	//IMPACTING THE PERFORMANCE OF THE INSTANCING PROCESS.
	int bufferID = 0;
	for (EntityBatch* entity : *batches)
	{
		std::vector<Instance> instanceData;
		std::multimap<Mesh*, Instance*> currentBatch = entity->GetInstanceBatch();

		Mesh* currentMesh = currentBatch.begin()->first;

		for (auto iterator = currentBatch.begin(); iterator != currentBatch.end(); iterator++)
		{
			//No need to add new instance types in an else as this is updating pre-existing
			//data rather than compiling new buffers.
			if (currentMesh == iterator->first)
			{
				//Same mesh, add to instance vector
				instanceData.push_back(*iterator->second);
			}
		}

		MapInstanceBuffer(currentMesh, &instanceData, bufferID++);
		instanceData.clear();
	}
}
#pragma endregion

//Creates an instance buffer ready for usage.
void GameManager::CreateInstanceBuffer(Mesh* mesh, std::vector<Instance>* instances)
{
	renderBuffers[RENDER_BUFFER_INSTANCE].emplace_back();
	D3D11_BUFFER_DESC instBuffDesc;
	ZeroMemory(&instBuffDesc, sizeof(instBuffDesc));

	instBuffDesc.Usage = D3D11_USAGE_DYNAMIC;
	instBuffDesc.ByteWidth = sizeof(Instance) * instances->size();
	instBuffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	instBuffDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	instBuffDesc.MiscFlags = 0;
	instBuffDesc.StructureByteStride = sizeof(Instance) * instances->size();

	D3D11_SUBRESOURCE_DATA instData;
	ZeroMemory(&instData, sizeof(instData));

	instData.pSysMem = &instances->at(0);
	instData.SysMemPitch = 0;
	instData.SysMemSlicePitch = 0;

	CheckFailWithError(dxManager.GetDevice()->CreateBuffer(&instBuffDesc, &instData, &renderBuffers[RENDER_BUFFER_INSTANCE][renderBuffers[RENDER_BUFFER_INSTANCE].size() - 1]),
		"An error occurred whilst building an instance buffer",
		"[GameManager]");

	renderBuffers[RENDER_BUFFER_VERTEX].push_back(mesh->GetBuffer(VERTEX_BUFFER));
}

//Creates an instance buffer ready for usage.
void GameManager::MapInstanceBuffer(Mesh* mesh, std::vector<Instance>* instances, int bufferID)
{
	if (buffersValid)
	{
		D3D11_MAPPED_SUBRESOURCE resource;
		HRESULT res = dxManager.GetContext()->Map(renderBuffers[RENDER_BUFFER_INSTANCE][bufferID], NULL,
			D3D11_MAP_WRITE_DISCARD, NULL, &resource);

		if (SUCCEEDED(res))
		{
			int i = 12;
			memcpy(resource.pData, &instances->at(0), sizeof(Instance) * instances->size());
			dxManager.GetContext()->Unmap(renderBuffers[RENDER_BUFFER_INSTANCE][bufferID], NULL);
		}
	}
}

bool GameManager::LoadShader(LPCSTR file, char* tag, Enumerations::ShaderType type)
{
	return mgrShader->AddShader(file, tag, type);
}

bool GameManager::LoadTexture(char* tag, LPCSTR fileName)
{
	return textureManager->AddTexture(tag, fileName);
}

bool GameManager::LoadMesh(LPCSTR tag, LPCSTR filename)
{
	return meshManager.LoadMesh(tag, filename);
}

bool GameManager::LoadEntity(std::string tag, LPCSTR mesh, LPCSTR vertexShader, LPCSTR hullShader,
	LPCSTR domainShader, LPCSTR geometryShader, LPCSTR pixelShader)
{
	return entityManager.AddEntity(tag, meshManager[mesh], mgrShader->GetVertexShader(vertexShader), mgrShader->GetHullShader(hullShader), mgrShader->GetDomainShader(domainShader), mgrShader->GetGeometryShader(geometryShader), mgrShader->GetPixelShader(pixelShader));
}

Entity* GameManager::GetEntity(LPCSTR tag)
{
	return entityManager[tag];
}

#pragma region Instance Affectors
//The mutators in this region affect the instance data, and therefore certain
//changes will affect the size of the buffers. This means that the relevant functions
//should invalidate the buffers and cause a refresh.

void GameManager::AssignTexture(LPCSTR mesh, LPCSTR texture)
{
	entityManager[mesh]->AssignTexture((*textureManager)[texture]);
	buffersValid = false;
}

#pragma endregion
#pragma region Private Functions
//Miscellaneous
#pragma endregion
