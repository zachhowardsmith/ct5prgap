#pragma once
#pragma region System Includes
#include <vector>
#pragma endregion
#pragma region Custom Includes
#include "Camera.h"
#include "DXManager.h"
#include "Instance.h"
#include "Vertex.h"
#include "ShaderManager.h"
#include "MeshManager.h"
#include "EntityManager.h"
#include "TextureManager.h"
#include "ShaderSignature.h"
#include "EntityBatch.h"
#include "Macros.h"
#pragma endregion

using namespace TypeDefinitions;

//This class acts as the interface between the engine and the user.
class GameManager
{
	//GameManager is used primarily for user interfacing. This allows the engine access to
	//core components of the GameManager without exposing them to the user.
	friend class GameWindow;

#pragma region Public Functions
public:
	GameManager(HWND hwnd);
	~GameManager();


	GameState GetInitializationState();

	//Extension functions to allow engine-like external use
	bool LoadShader(LPCSTR file, char* tag, Enumerations::ShaderType type);
	bool LoadTexture(char* tag, LPCSTR fileName);
	bool LoadMesh(LPCSTR tag, LPCSTR filename);
	bool LoadEntity(std::string tag, LPCSTR mesh, LPCSTR vertexShader, LPCSTR hullShader,
		LPCSTR domainShader, LPCSTR geometryShader, LPCSTR pixelShader);


	template<typename T> void CreateConstantBuffer(ID3D11Buffer** buffer);
	void GameManager::CreateConstantBuffer(int size, ID3D11Buffer** buffer);

	Entity* GetEntity(LPCSTR tag);
	void AssignTexture(LPCSTR mesh, LPCSTR texture);
#pragma endregion

#pragma region Private Functions
private:
	void Render();
	void GenerateInstances();
	void FlushBuffers();
	void CompileInstanceBatches(bool createFresh);
	void BuildInstanceBuffers(std::vector<EntityBatch*>* batches);
	void UpdateInstanceBuffers(std::vector<EntityBatch*>* batches);
	void CreateInstanceBuffer(Mesh* mesh, std::vector<Instance>* instances);
	void MapInstanceBuffer(Mesh* mesh, std::vector<Instance>* instances, int bufferID);

#pragma endregion

#pragma region Public Variables

#pragma endregion

#pragma region Private Variables
private:
	XMMATRIX ALIGN projMatrix;
	Camera camera;
	DXManager dxManager;
	MeshManager meshManager;
	EntityManager entityManager;
	ShaderManager* mgrShader;
	TextureManager* textureManager;


	ID3D11Buffer* viewProjectionCB;
	ID3D11Buffer* cameraPositionCB;
	ID3D11Buffer* cameraDirectionCB;

	std::vector<EntityBatch*> uniqueBatches;
	std::vector<int> instanceCounts;
	std::vector<int> instanceIndexCounts;
	std::vector<ShaderSignature> uniqueSigs;

	//[0] = Vertex Buffers, [1] = Index Buffers, [2] = Instance Buffers
	std::vector<ID3D11Buffer*> renderBuffers[3];
	bool buffersValid;
#pragma endregion
};